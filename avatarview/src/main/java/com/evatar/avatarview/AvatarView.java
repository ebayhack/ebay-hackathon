package com.evatar.avatarview;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by miki on 11/20/15.
 */
public class AvatarView extends RelativeLayout {

    public static final String AVATAR_HEAD = "head";
    public static final String AVATAR_TORSO = "torso";
    public static final String AVATAR_LEGS = "legs";
    public static final String AVATAR_SHOES = "shoes";

    public static final String AVATAR_RIGHT_HAND_EXTRA = "rightHandExtra";
    public static final String AVATAR_LEFT_HAND_EXTRA = "leftHandExtra";

    public static final String BUNDLE_EXTRA_HEAD_IMAGE = "avatarExtraHead";
    public static final String BUNDLE_EXTRA_TORSO_IMAGE = "avatarExtraTorso";
    public static final String BUNDLE_EXTRA_LEGS_IMAGE = "avatarExtraLegs";
    public static final String BUNDLE_EXTRA_SHOES_IMAGE = "avatarExtraShoes";

    private LayoutInflater mInflater;

    private RelativeLayout mHeadWrapper, mTorsoWrapper, mLegsWrapper, mShoesWrapper;
    private ImageView mHeadIV, mTorsoIV, mLegsIV, mShoesIV;
    private ImageView mHeadOverlayIV, mTorsoOverlayIV, mLegsOverlayIV, mShoesOverlayIV;

    private Map<String, ImageView> mExtraImageMap;

    private String skinColor;

    public AvatarView(Context context) {
        super(context);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public String getAvatarSkinColor(){ return skinColor; }

    public void setAvatarSkinColor(String skinColorHex) {


        try {
            skinColor = skinColorHex;
            mHeadIV.setColorFilter(Color.parseColor(skinColorHex), PorterDuff.Mode.SRC_ATOP);
            mTorsoIV.setColorFilter(Color.parseColor(skinColorHex), PorterDuff.Mode.SRC_ATOP);
            mLegsIV.setColorFilter(Color.parseColor(skinColorHex), PorterDuff.Mode.SRC_ATOP);

        } catch (Exception e) {

            Log.e("AvatarView", "failed to change skin color");

        }
    }

    public ImageView getHeadView() { return mHeadOverlayIV; }

    public void setAvatarPiece(Bitmap piece, String tag, boolean animate) {
        switch(tag) {
            case AVATAR_HEAD:
                setHeadBitmap(piece, animate);
                break;
            case AVATAR_TORSO:
                setTorsoBitmap(piece, animate);
                break;
            case AVATAR_LEGS:
                setLegsBitmap(piece, animate);
                break;
            case AVATAR_SHOES:
                setShoesBitmap(piece, animate);
                break;
        }
    }

    public ImageView getAvatarPieceImage(String tag) {
        switch(tag) {
            case AVATAR_HEAD:
                return mHeadOverlayIV;
            case AVATAR_TORSO:
                return mTorsoOverlayIV;
            case AVATAR_LEGS:
                return mLegsOverlayIV;
            case AVATAR_SHOES:
                return mShoesOverlayIV;
        }
        return getExtraImageView(tag);
    }

    public void setHeadBitmap(Bitmap headpiece, boolean animate) {
        if(headpiece == null) return; //TODO Throws exception
        Bitmap bmp = ((BitmapDrawable)mHeadIV.getDrawable()).getBitmap();
        float ratio = (float)bmp.getWidth() / headpiece.getWidth();
        mHeadOverlayIV.setImageBitmap(Bitmap.createScaledBitmap(headpiece, bmp.getWidth(), (int)(headpiece.getHeight() * ratio), false));
    }

    public ImageView getTorsoView() { return mHeadOverlayIV; }
    public void setTorsoBitmap(Bitmap torso, boolean animate) {
        if(torso == null) return; //TODO Throws exception
        Bitmap bmp = ((BitmapDrawable)mTorsoIV.getDrawable()).getBitmap();
        float ratio = (float)bmp.getWidth() / torso.getWidth();
        mTorsoOverlayIV.setImageBitmap(Bitmap.createScaledBitmap(torso, bmp.getWidth(), (int) (torso.getHeight() * ratio), false));
    }
    public ImageView getLegsView() { return mHeadOverlayIV; }
    public void setLegsBitmap(Bitmap legs, boolean animate) {
        if(legs == null) return; //TODO Throws exception
        Bitmap bmp = ((BitmapDrawable)mLegsIV.getDrawable()).getBitmap();
        float ratio = (float)bmp.getWidth() / legs.getWidth();
        mLegsOverlayIV.setImageBitmap(Bitmap.createScaledBitmap(legs, bmp.getWidth(), (int)(legs.getHeight() * ratio), false));
    }

    public ImageView getShoesView() { return mHeadOverlayIV; }
    public void setShoesBitmap(Bitmap shoes, boolean animate) {
        if(shoes == null) return; //TODO Throws exception
        Bitmap bmp = ((BitmapDrawable)mShoesIV.getDrawable()).getBitmap();
        float ratio = (float)bmp.getWidth() / shoes.getWidth();
        mShoesOverlayIV.setImageBitmap(Bitmap.createScaledBitmap(shoes, bmp.getWidth(), (int)(shoes.getHeight() * ratio), false));
    }

    public @Nullable ImageView getExtraImageView(String tag) {
        return mExtraImageMap.get(tag);
    }

    public void setExtraImageView(String tag, Bitmap image) {
        ImageView iv = getExtraImageView(tag);
        if(iv != null) {
            iv.setImageBitmap(image);
            iv.setVisibility(VISIBLE);
        }
    }

    public void removeExtraImageView(String tag) {
        ImageView iv = getExtraImageView(tag);
        if(iv != null) {
            iv.setVisibility(GONE);
        }
    }

    private void initView(Context context){
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View.inflate(context, R.layout.avatar_view, this);
        mInflater.inflate(R.layout.avatar_view, this, true);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mHeadWrapper = (RelativeLayout)findViewById(R.id.avatar_head_wrapper);
        mTorsoWrapper = (RelativeLayout)findViewById(R.id.avatar_torso_wrapper);
        mLegsWrapper = (RelativeLayout)findViewById(R.id.avatar_legs_wrapper);
        mShoesWrapper = (RelativeLayout)findViewById(R.id.avatar_shoes_wrapper);

        mHeadIV = (ImageView)mHeadWrapper.findViewById(R.id.head_image_view);
        mTorsoIV = (ImageView)mTorsoWrapper.findViewById(R.id.torso_image_view);
        mLegsIV = (ImageView)mLegsWrapper.findViewById(R.id.legs_image_view);
        mShoesIV = (ImageView)mShoesWrapper.findViewById(R.id.shoes_image_view);

        mHeadOverlayIV = (ImageView)mHeadWrapper.findViewById(R.id.head_over_image_view);
        mTorsoOverlayIV = (ImageView)mTorsoWrapper.findViewById(R.id.torso_over_image_view);
        mLegsOverlayIV = (ImageView)mLegsWrapper.findViewById(R.id.legs_over_image_view);
        mShoesOverlayIV = (ImageView)mShoesWrapper.findViewById(R.id.shoes_over_image_view);

        mExtraImageMap = new HashMap<>();
        mExtraImageMap.put(AVATAR_RIGHT_HAND_EXTRA, (ImageView)findViewById(R.id.avatar_extra_right_hand_image_view));
        mExtraImageMap.put(AVATAR_LEFT_HAND_EXTRA, (ImageView)findViewById(R.id.avatar_extra_left_hand_image_view));
    }

    private void init() {
        initView(getContext());
        setClipChildren(false);
    }

    public ViewPropertyAnimator animate(final SimpleAnimateListener listener) {
        return animate().setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                listener.onStarted();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                listener.onFinished();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public interface SimpleAnimateListener {
        void onFinished();
        void onStarted();
    }

    //    @Override
//    protected void onLayout(boolean changed, int l, int t, int r, int b) {
//        LayoutParams params = (RelativeLayout.LayoutParams)getLayoutParams();
//        for (int i = 0; i < getChildCount(); i++) {
//            View child = getChildAt(i);
////            getChildAt(i).layout(l, t, r, b);
//            getChildAt(i).layout(l - params.leftMargin, t - params.topMargin, r - params.rightMargin, b - params.bottomMargin);
////            LayoutParams layoutParams = (LayoutParams)getChildAt(0).getLayoutParams();
////            layoutParams.addRule(CENTER_IN_PARENT, params.getRule(CENTER_IN_PARENT));
//        }
//    }
}
