package com.evatar;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.evatar.facebook.FacebookFriend;
import com.evatar.facebook.FacebookGraphAPI;
import com.evatar.facebook.FriendListAdapter;
import com.evatar.parse.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.parse.ParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AddFriendsActivity extends BaseActivity implements GraphRequest.Callback, FacebookCallback<LoginResult>, AdapterView.OnItemClickListener{

    private final String FACEBOOK_FRIENDS_TOKEN = "/me/friends";

    @Bind(R.id.toolbar_app_bar_layout)
    AppBarLayout appBarLayout;

    @Bind(R.id.find_friends_list_view)
    ListView mAddFriendsLV;

//    @Bind(R.id.login_button)
    LoginButton mFacebookLoginButton;
    CallbackManager mCallbackManager;

    @Bind(R.id.stv)
    TextView stv;

    MainApplication app;
    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (MainApplication)getApplication();
        mUser = app.getCurrentUser();
        if(mUser == null) {
            finish(); //TODO Raise exception
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_add_friends);
        initAppCompatDesign(appBarLayout);
        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);

        mCallbackManager = CallbackManager.Factory.create();

        if(!isLoggedIn()) {

            mFacebookLoginButton.setReadPermissions("user_friends", "user_likes", "user_photos");
            mFacebookLoginButton.registerCallback(mCallbackManager, this);

        } else {
            onLogin();
        }

        ViewUtils.overrideFonts(this, appBarLayout, "fonts/gabriel.otf");
    }

    private void onLogin() {
        GraphRequest.Callback callback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse graphResponse) {
                mFacebookLoginButton.setVisibility(View.GONE);
                try {

                    JSONArray friends = graphResponse.getJSONObject().getJSONArray("data");
                    initList(friends);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                    Snackbar.make(appBarLayout, graphResponse.getRawResponse(), Snackbar.LENGTH_LONG).show();
                }
            }
        };
        String request = FacebookGraphAPI.generateCall(FacebookGraphAPI.ME, FacebookGraphAPI.FRIENDS);
        new GraphRequest(AccessToken.getCurrentAccessToken(), request, null, HttpMethod.GET, callback).executeAsync();
    }

    private void initList(final JSONArray friends) {
        final List<FacebookFriend> friendList = new ArrayList<>();
        final List<String> friendIds = mUser.getUserFacebookFriends();
        //Parse the JSONArray of friends into the FacebookFriend model to use in a listview
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                for(int i = 0; i < friends.length(); i++) {
                    try {
                        JSONObject friendJson = friends.getJSONObject(i);
                        String id = friendJson.getString("id");
                        if(friendIds.contains(id)) {
                            continue;
                        }
                        String name = friendJson.getString("name");
                        Bundle bundle = new Bundle();
                        bundle.putInt("redirect", 0);
                        String request = FacebookGraphAPI.generateUserCall(id, FacebookGraphAPI.PROFILE_PICTURE);
                        GraphResponse friendResponse = new GraphRequest(AccessToken.getCurrentAccessToken(), request, bundle, HttpMethod.GET).executeAndWait();
                        Log.d("JSON", friendResponse.getRawResponse());
                        String photoUrl = friendResponse.getJSONObject().getJSONObject("data").getString("url");
                        friendList.add(new FacebookFriend(name, id, photoUrl));
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        //Snackbar.make(mAddFriendsLV, ex.getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mAddFriendsLV.setEmptyView(stv);
                mAddFriendsLV.setAdapter(new FriendListAdapter(AddFriendsActivity.this, friendList));
                mAddFriendsLV.setOnItemClickListener(AddFriendsActivity.this);
                mAddFriendsLV.setVisibility(View.VISIBLE);
                Toast.makeText(AddFriendsActivity.this, "Success: " + friendList.size(), Toast.LENGTH_LONG).show();
            }
        }.execute(null, null, null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FacebookFriend friend = ((FriendListAdapter)parent.getAdapter()).getItem(position);
        addFriend(friend.getId());
    }

    private void addFriend(String friendId) {
        mUser.addUserFacebookFriend(friendId);

    }

    @Override
    public void onBackPressed() {
        try {
            mUser.save();
        } catch (ParseException ex) {
            ex.printStackTrace(); //TODO Handle
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        final String userId = loginResult.getAccessToken().getUserId();
        onLogin();

    }

    @Override public void onCancel() { }

    @Override public void onError(FacebookException e) { }

    @Override
    public void onCompleted(GraphResponse graphResponse) {
//        Snackbar.make(this.stv, graphResponse.getRawResponse(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void initViews() {
        mFacebookLoginButton = (LoginButton)findViewById(R.id.login_button);
        ButterKnife.bind(this);
        ViewUtils.overrideFonts(this, appBarLayout, "fonts/gabriel.otf");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_search:
                //TODO Init search
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
