package com.evatar;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.evatar.ebayapi.EbayItem;
import com.evatar.ebayapi.EbayUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by miki on 11/13/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{




    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private Context mContext;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

//    private HeaderViewHolder mHeaderViewHolder;

    private String[] mCategoryNames = {"Hats", "Pants", "Shirts", "Shoes"};
    private final String[] CATEGORY_KEYS = {"head", "legs", "torso", "shoes"};

    private List<ItemListAdapter> adapters;

    private RecyclerViewAdapter() {}

    public RecyclerViewAdapter(Context ctx) {
        mContext = ctx;
        adapters = new ArrayList<>();
        for(int i = 0; i < CATEGORY_KEYS.length;i++) {
            List<ParseObject> obj = null;
            try {
                obj = ParseQuery.getQuery("Items").whereEqualTo("item_type", CATEGORY_KEYS[i]).find();

            }catch (ParseException ex) {
                ex.printStackTrace();
            }
            //adapters.add(new ItemListAdapter(mContext, obj));
        }
    }

    @SuppressLint("NewApi") @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        if (type == TYPE_ITEM) {
            View itemView = inflater.inflate(R.layout.layout_list_item, viewGroup, false);
            return new ViewHolder(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + type + " + make sure you're using types correctly");
    }

    private Bitmap getBitmapFromAsset(String rootFolder, String filePath) {
        AssetManager assetManager = mContext.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(rootFolder + "/" + filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        sp = mContext.getSharedPreferences("object" , Context.MODE_PRIVATE);
        editor = sp.edit();

        if (false) {
            final ViewHolder vh = ((ViewHolder) holder);
            vh.productName.setText(mCategoryNames[position]);
            Bitmap bmp = getBitmapFromAsset("categories", String.valueOf(position + 1) + ".png");
            if(bmp != null) {
                vh.productImage.setImageBitmap(bmp);
//                try {
                    ItemListAdapter adapter = adapters.get(position);
//                    vh.productListView.setAdapter(adapter);
//                    vh.productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                            ParseObject object = ((ItemListAdapter)parent.getAdapter()).getItem(position);
////                            HeaderViewHolder header = getHeaderViewHolder();
//                            switch (object.getString("item_type")) {
//                                case AvatarView.AVATAR_HEAD:
////                                    header.avatarView.setHair(object.getString("asset_name"));
//                                    editor.putString("AVATAR_HEAD", object.getString("asset_name"));
//                                    editor.apply();
//                                    break;
//                                case AvatarView.AVATAR_LEGS:
////                                    header.avatarView.setLegs(object.getString("asset_name"));
//                                    editor.putString("AVATAR_LEGS", object.getString("asset_name"));
//                                    editor.apply();
//                                    break;
//                                case AvatarView.AVATAR_SHOES:
////                                    header.avatarView.setShoes(object.getString("asset_name"));
//                                    editor.putString("AVATAR_SHOES", object.getString("asset_name"));
//                                    editor.apply();
//                                    break;
//                                case AvatarView.AVATAR_TORSO:
////                                    header.avatarView.setTorso(object.getString("asset_name"));
//                                    editor.putString("AVATAR_TORSO", object.getString("asset_name"));
//                                    editor.apply();
//                                    break;
//
//                            }
////                            mContext.startActivity(new Intent(mContext, CheckoutActivity.class));
//                        }
//                    });
//                    setListViewHeightBasedOnChildren(vh.productListView);
//                } catch (ParseException ex) {
//                    ex.printStackTrace();
//                }
            }
//        } else if (holder instanceof HeaderViewHolder) {
            //cast holder to HeaderViewHolder and set data for header.
//            final HeaderViewHolder vh = (HeaderViewHolder)holder;
//            ParseQuery.getQuery(User.class).getInBackground("o7WfTZ8CDT", new GetCallback<User>() {
//                @Override
//                public void done(User object, ParseException e) {
//                    if (e == null) {
//                        if(object.getLevelMaxPoints() <= object.getUserPoints()) {
//                            object.increment("level");
//                            object.saveInBackground();
//                        }
//                        vh.mUser = object;
//                        vh.mUsernameTv.setText(vh.mUser.getUsername());
//                        int uPoints = vh.mUser.getUserPoints();
//                        int levelMaxPoints = vh.mUser.getLevelMaxPoints();
//                        vh.mPointsTv.setText(uPoints + "/" + levelMaxPoints);
//                        vh.mPointsProgressBar.setProgress(100 * uPoints / levelMaxPoints);
//                        vh.mLevelTv.setText("Level: " + vh.mUser.getLevel());
//                    } else {
//                        e.printStackTrace();
//                    }
//                }
//            });
        }
    }

//    public HeaderViewHolder getHeaderViewHolder() {
//        return mHeaderViewHolder;
//    }

    @Override
    public int getItemViewType(int position) {
//        if (position == 0)
//            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView productImage;
        public TextView productName;
//        public ListView productListView;
        public ImageView arrowImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            productName = (TextView)itemView.findViewById(R.id.card_product_name_textview);
            productImage = (ImageView)itemView.findViewById(R.id.card_product_image_view);
//            productListView = (ListView)itemView.findViewById(R.id.card_product_list);
            arrowImageView = (ImageView)itemView.findViewById(R.id.card_arrow_image);
//            setListViewHeightBasedOnChildren(productListView);
//            itemView.setOnClickListener(this);
//            onClick(itemView);
        }

        public void animateItem(final View item) {
            ViewPropertyAnimator animator = item.animate().rotationBy(180).setListener(new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animation) { }
                @Override public void onAnimationEnd(Animator animation) { }
                @Override public void onAnimationCancel(Animator animation) { }
                @Override public void onAnimationRepeat(Animator animation) { }
            });
            animator.setInterpolator(new DecelerateInterpolator());
            animator.setDuration(200);
            animator.start();
        }

//        @Override
//        public void onClick(View v) {
//            if(productListView.getVisibility() == View.GONE) {
////                ViewUtils.expand(productListView);
//
//                productListView.setVisibility(View.VISIBLE);
//                productListView.animate().scaleY(1).setDuration(150).setListener(new Animator.AnimatorListener() {
//                    @Override public void onAnimationStart(Animator animation) {  }
//                    @Override public void onAnimationEnd(Animator animation) {
//                        productListView.setScaleY(1);
//                    }
//                    @Override public void onAnimationCancel(Animator animation) { }
//                    @Override public void onAnimationRepeat(Animator animation) { }
//                }).start();
//            } else {
////                ViewUtils.collapse(productListView);
//                productListView.animate().scaleY(0).setDuration(150).setListener(new Animator.AnimatorListener() {
//                    @Override public void onAnimationStart(Animator animation) {  }
//                    @Override public void onAnimationEnd(Animator animation) {
//                        productListView.setScaleY(0);
//                        productListView.setVisibility(View.GONE);
//                    }
//                    @Override public void onAnimationCancel(Animator animation) { }
//                    @Override public void onAnimationRepeat(Animator animation) { }
//                }).start();
//            }
//            animateItem(arrowImageView);
//        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

//    class HeaderViewHolder extends RecyclerView.ViewHolder {
//
//        public User mUser;
//
//        public TextView mUsernameTv, mPointsTv, mLevelTv;
//        public ProgressBar mPointsProgressBar;
//        public AvatarView avatarView;
//
//        public HeaderViewHolder(View itemView) {
//            super(itemView);
//            mUsernameTv = (TextView)itemView.findViewById(R.id.avatar_username_textview);
//            mPointsTv = (TextView)itemView.findViewById(R.id.avatar_points_next_level_textview);
//            mPointsProgressBar = (ProgressBar)itemView.findViewById(R.id.avatar_points_progressbar);
//            mLevelTv = (TextView)itemView.findViewById(R.id.avatar_level_textview);
//            avatarView = (AvatarView)itemView.findViewById(R.id.avatar_view);
//        }
//    }

    public static class ItemListAdapter extends BaseAdapter {

        private List<EbayItem> mData;
        private List<Bitmap> imageData;
        private Context mContext;

        public ItemListAdapter(Context ctx, List<EbayItem> data) {
            mData = data;
            mContext = ctx;
            imageData = new ArrayList<>(data.size());
            for(int i = 0 ;i < data.size();i++) {
                imageData.add(null);
            }
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public EbayItem getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return Long.valueOf(mData.get(position).getItemId());
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item, null);

            }

            final int IMAGE_SIZE = 56;

            TextView pName = (TextView)convertView.findViewById(R.id.list_product_name_textview);
            pName.setText(getItem(position).getTitle());
            TextView pRetailer = (TextView)convertView.findViewById(R.id.list_product_retailer_textview);
            String brand = getItem(position).getItemSpecifics().get("Brand")[0];
            pRetailer.setText(brand == null ? "Unbranded" : brand);
            TextView pPrice = (TextView)convertView.findViewById(R.id.list_product_price);
            pPrice.setText(String.valueOf(EbayUtils.formatPrice(getItem(position).getCountrySymbol(), getItem(position).getPrice())));

            final ImageView productImageView = (ImageView)convertView.findViewById(R.id.list_product_image);
            if(getItem(position).getPictureUrls().length > 0) {
                final String path = getItem(position).getPictureUrls()[0];
                if (path != null && imageData.get(position) == null) {
                    new AsyncTask<Void, Void, RequestCreator>() {
                        @Override
                        protected RequestCreator doInBackground(Void... params) {
                            return Picasso.with(mContext).load(path).resize(IMAGE_SIZE, IMAGE_SIZE);
                        }

                        @Override
                        protected void onPostExecute(RequestCreator requestCreator) {
                            requestCreator.into(productImageView);
                        }
                    }.execute(null, null, null);
                } else if (imageData.get(position) != null) {
                    productImageView.setImageBitmap(imageData.get(position));
                }
            }

            return convertView;
        }
    }
}
