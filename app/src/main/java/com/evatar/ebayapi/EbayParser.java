package com.evatar.ebayapi;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.evatar.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EbayParser {

    private final static String TAG = "EbayParser";

    private static SimpleDateFormat dateFormat;
    private Resources mResources;

    public EbayParser(Context context) {

        synchronized (this) {
            if (dateFormat == null) {
                dateFormat = new SimpleDateFormat("[\"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\"]");
            }
        }
        this.mResources = context.getResources();
    }

    private ArrayList<EbayListing> parseSearchListings(String jsonResponse) {
        ArrayList<EbayListing> listings = new ArrayList<>();

        JSONArray itemList;
        try {
            JSONObject rootObj=new JSONObject(jsonResponse);
            itemList = rootObj
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_findItemsByKeywordsResponse))
                    .getJSONObject(0)
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_searchResult))
                    .getJSONObject(0)
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_item));
        } catch(JSONException ex) {
            Log.e(TAG, "parseListings itemList: " + ex);
            return new ArrayList<>();
        }

        int itemCount=itemList.length();

        for(int itemIndex=0;itemIndex<itemCount;itemIndex++){
            try{
                EbayListing listing=this.parseListing(itemList.getJSONObject(itemIndex));
                listing.setAuctionSource(this.mResources.getString(R.string.ebay_source_name));
                listings.add(listing);
            } catch(JSONException jx) {
                /* if something goes wrong log, move to the next item */
                Log.e(TAG,"parseListings: jsonResponse="+jsonResponse,jx);
            }
        }
        return listings;
    }

    private ArrayList<EbayListing> parseCategoryListings(String jsonResponse) throws JSONException{
        ArrayList<EbayListing> listings = new ArrayList<>();

        JSONArray itemList;
        try {
            JSONObject rootObj = new JSONObject(jsonResponse);
            itemList = rootObj
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_findItemsByCategoryResponse))
                    .getJSONObject(0)
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_searchResult))
                    .getJSONObject(0)
                    .getJSONArray(this.mResources.getString(R.string.ebay_tag_item));
        } catch(JSONException ex) {
            Log.e(TAG, "parseListings itemList: " + jsonResponse);
            throw ex;
        }

        int itemCount=itemList.length();

        for(int itemIndex=0;itemIndex<itemCount;itemIndex++){
            try{
                EbayListing listing=this.parseListing(itemList.getJSONObject(itemIndex));
                listing.setAuctionSource(this.mResources.getString(R.string.ebay_source_name));
                listings.add(listing);
            } catch(JSONException jx) {
                /* if something goes wrong log, move to the next item */
                Log.e(TAG,"parseListings: jsonResponse="+jsonResponse,jx);
            }
        }
        return listings;
    }

    public ArrayList<EbayListing> parseListings(EbayRequest request){

        String jsonResponse;
        try {
            jsonResponse = request.invokeResponse();
        } catch(Exception ex) {
            ex.printStackTrace();
            throw new IllegalArgumentException("Illegal EbayRequest JSON");
        }

        if(request instanceof EbaySearchRequest) {
            return parseSearchListings(jsonResponse);
        } else if(request instanceof EbaySearchCategoryRequest) {
            try {
                return parseCategoryListings(jsonResponse);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        throw new IllegalArgumentException("Illegal EbayRequest");

    }

    private EbayListing parseListing(JSONObject jsonObj) throws JSONException{
        /*
         * Things outside of a try/catch block are fields that are required and should throw an exception if not found
         * Things inside of a try/catch block are fields we can live without
         */
        EbayListing listing=new EbayListing();

        /* get items at the root of the object
         * id, title, and URL are required
         * image and location are optional */

        listing.setId(jsonObj.getString(this.mResources.getString(R.string.ebay_tag_itemId)));
        listing.setTitle(this.stripWrapper(jsonObj.getString(this.mResources.getString(R.string.ebay_tag_title))));
        listing.setListingUrl(this.stripWrapper(jsonObj.getString(this.mResources.getString(R.string.ebay_tag_viewItemURL))));

        try {
            listing.setImageUrl(this.stripWrapper(jsonObj.getString(this.mResources.getString(R.string.ebay_tag_galleryURL))));
        } catch(JSONException jx){
//            Log.w(TAG,"parseListing: parsing image URL",jx);
            listing.setImageUrl(null);
        }

        try {
            listing.setLocation(this.stripWrapper(jsonObj.getString(this.mResources.getString(R.string.ebay_tag_location))));
        } catch(JSONException jx){
//            Log.w(TAG,"parseListing: parsing location",jx);
            listing.setLocation(null);
        }

        //get stuff under sellingStatus - required
        JSONObject sellingStatusObj = jsonObj.getJSONArray(this.mResources.getString(R.string.ebay_tag_sellingStatus)).getJSONObject(0);
        JSONObject currentPriceObj = sellingStatusObj.getJSONArray(this.mResources.getString(R.string.ebay_tag_currentPrice)).getJSONObject(0);

        listing.setCurrentPrice(this.formatCurrency(currentPriceObj.getString(this.mResources.getString(R.string.ebay_tag_value)),
                currentPriceObj.getString(this.mResources.getString(R.string.ebay_tag_currencyId))));

        //get stuff under shippingInfo - optional
        try {
            JSONObject shippingInfoObj = jsonObj.getJSONArray(this.mResources.getString(R.string.ebay_tag_shippingInfo)).getJSONObject(0);
            JSONObject shippingServiceCostObj = shippingInfoObj.getJSONArray(this.mResources.getString(R.string.ebay_tag_shippingServiceCost)).getJSONObject(0);

            listing.setShippingCost(this.formatCurrency(
                    shippingServiceCostObj.getString(this.mResources.getString(R.string.ebay_tag_value)),
                    currentPriceObj.getString(
                    this.mResources.getString(R.string.ebay_tag_currencyId))));
        } catch(JSONException jx){
//            Log.w(TAG,"parseListing: parsing shipping cost",jx);
            listing.setShippingCost("Not listed");
        }

        //get stuff under listingInfo

        try {
            JSONObject listingInfoObj=jsonObj.getJSONArray(this.mResources.getString(R.string.ebay_tag_listingInfo)).getJSONObject(0);
            try {

                String listingType=this.stripWrapper(listingInfoObj.getString(this.mResources.getString(R.string.ebay_tag_listingType)));

                if(listingType.toLowerCase().contains(this.mResources.getString(R.string.ebay_value_auction))){

                    listing.setAuction(true);
                    try {
                        String buyItNowAvailable = this.stripWrapper(listingInfoObj.getString(this.mResources.getString(R.string.ebay_tag_buyItNowAvailable)));

                        if(buyItNowAvailable.equalsIgnoreCase(this.mResources.getString(R.string.ebay_value_true))){
                            listing.setBuyItNow(true);
                        } else listing.setBuyItNow(false);
                    } catch(JSONException jx){
//                        Log.e(TAG,"parseListing: parsing but it now",jx);
                    }

                } else {
                    listing.setAuction(false);
                    listing.setBuyItNow(true);
                }

            }catch(JSONException jx){
//                Log.e(TAG,"parseListing: parsing listing type",jx);
            }

            //get start and end dates - optional

            try{

                Date startTime=dateFormat.parse(listingInfoObj.getString(this.mResources.getString(R.string.ebay_tag_startTime)));

                listing.setStartTime(startTime);

                Date endTime=dateFormat.parse(listingInfoObj.getString(this.mResources.getString(R.string.ebay_tag_endTime)));

                listing.setEndTime(endTime);

            } catch(Exception x) { //generic - both ParseException and JSONException can be thrown, same result either way
//                Log.e(TAG, "parseListing: parsing start and end dates", x);
                listing.setStartTime(null);
                listing.setEndTime(null);
            }
        } catch(JSONException jx) {
//            Log.e(TAG,"parseListing: parsing listing info",jx);
            listing.setStartTime(null);
            listing.setEndTime(null);
        }

        //alright, all done

        return(listing);

    }

    private String formatCurrency(String amount,String currencyCode){

        StringBuffer formattedText=new StringBuffer(amount);
        try{
            //add trailing zeros
            int indexOf=formattedText.indexOf(".");
            if(indexOf>=0){
                if(formattedText.length()-indexOf==2){
                    formattedText.append("0");
                }

            }

            //add dollar sign
            if(currencyCode.equalsIgnoreCase("USD")) {
                formattedText.insert(0,"$");
            } else {
                formattedText.append(" ");

                formattedText.append(currencyCode);

            }

        }catch(Exception x){

            Log.e(TAG,"formatCurrency",x);

        }

        return(formattedText.toString());

    }

    private String stripWrapper(String s){

        try{

            int end=s.length()-2;

            return(s.substring(2,end));

        }catch(Exception x){

            Log.e(TAG,"stripWrapper",x);

            return(s);

        }

    }
}