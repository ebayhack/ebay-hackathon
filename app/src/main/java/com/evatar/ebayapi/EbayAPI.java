package com.evatar.ebayapi;

import android.content.Context;

import com.evatar.R;

/**
 * Created by miki on 11/27/15.
 */
public class EbayAPI {
    public static final String SANDBOX = "typeSandbox";
    public static final String PRODUCTION = "typeProduction";

    private static String mAppId;
    private static String mBaseRequestUrl;
    private static Context mContext;

    public static void initialize(Context c, String apiType) {
        mContext = c;
        switch(apiType) {
            case SANDBOX:
                mBaseRequestUrl = c.getResources().getString(R.string.ebay_wsurl_sandbox);
                mAppId = c.getResources().getString(R.string.ebay_appid_sandbox);
                break;
            case PRODUCTION:
                mBaseRequestUrl = c.getResources().getString(R.string.ebay_wsurl_production);
                mAppId = c.getResources().getString(R.string.ebay_appid_production);
                break;
            default:
                throw new IllegalArgumentException("apiType must be either PRODUCTION of SANDBOX");
        }

    }

    public static String getAppId() {
        if(mAppId == null) {
            throw new NotInitializedException();
        }
        return mAppId;
    }

    public static Context getApplicationContext() {
        if(mContext == null) {
            throw new NotInitializedException();
        }
        return mContext;
    }

    public static String getBaseRequestUrl() {
        if(mBaseRequestUrl == null) {
            throw new NotInitializedException();
        }
        return mBaseRequestUrl;
    }

    private static class NotInitializedException extends IllegalStateException {
        public NotInitializedException() {
            super("Ebay API not initialized");
        }
    }
}
