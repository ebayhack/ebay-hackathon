package com.evatar.ebayapi;

/**
 * Created by miki on 12/13/15.
 */
public class EbayUtils {
    public static String formatPrice(String countrySymbol, double price) {
        StringBuilder builder = new StringBuilder();

        String symbol = "";
        switch (countrySymbol) {
            case "USD":
                symbol = "$";
                break;
            case "GBP":
                symbol = "£";
                break;
            case "AUD":
                symbol = "$AU";
                break;
            case "EUR":
                symbol = "€";
                break;
            case "NIS":
                symbol = "₪";
                break;
            default:
                symbol = "$";
        }
        builder.append(symbol).append(price);
        return builder.toString();
    }
}
