package com.evatar.ebayapi;


import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;

public class EbaySearchResult {
    public final static int RESULT_SUCCESS = 0;
    public final static int RESULT_ERROR = 1;

    private int mResultCode;
    private Exception mError;
    private ArrayList<EbayListing> mListings;

    public int getResultCode() {
        return mResultCode;
    }

    public void setResultCode(int resultCode) {
        this.mResultCode = resultCode;
    }

    public Exception getError() {
        return mError;
    }

    public void setError(Exception error) {
        this.mError = error;
    }

    public ArrayList<EbayListing> getListings() {
        return mListings;
    }

    public void setListings(ArrayList<EbayListing> listings) {
        this.mListings = listings;
    }

    public EbaySearchResult() {
        this.mListings = new ArrayList<>();
    }

    @Deprecated
    public EbaySearchResult(ArrayList<EbayListing> listings) {
        this.mListings = listings;
    }

    public EbaySearchResult(Context ctx, EbayRequest invoke) {
        EbayParser parser = new EbayParser(ctx);
        try {
            this.mListings = parser.parseListings(invoke);
        } catch (Exception ex) {
            //TODO Handle
            ex.printStackTrace();
            this.mListings = null;
        }
    }


    public void append(EbaySearchResult toAppend){
        this.mListings.addAll(toAppend.getListings());
    }

    public void sort(){ Collections.sort(this.mListings); }

}
