package com.evatar.ebayapi;

import android.support.annotation.NonNull;

import java.util.Date;

public class EbayListing implements Comparable<EbayListing>{

    private String mId;
    private String mTitle;
    private String mImageUrl, mListingUrl;
    private String mLocation;
    private String mShippingCost;
    private String mCurrentPrice;
    private String mAuctionSource;
    private Date mStartTime, mEndTime;
    private boolean auction;
    private boolean buyItNow;

    public String getId() { return mId; }
    public void setId(String id) { mId = id; }

    public String getTitle() { return mTitle; }
    public void setTitle(String title) { mTitle = title; }
    
    public String getImageUrl() { return mImageUrl; }
    public void setImageUrl(String url) { mImageUrl = url; }

    public String getListingUrl() { return mListingUrl; }
    public void setListingUrl(String url) { mListingUrl = url; }

    public String getLocation() { return mLocation; }
    public void setLocation(String location) { mLocation = location; }

    public String getShippingCost() { return mShippingCost; }
    public void setShippingCost(String cost) { mShippingCost = cost; }

    public String getCurrentPrice() { return mCurrentPrice; }
    public void setCurrentPrice(String price) { mCurrentPrice = price; }

    public String getAuctionSource() { return mAuctionSource; }
    public void setAuctionSource(String source) { mAuctionSource = source; }

    public Date getStartTime() { return mStartTime; }
    public void setStartTime(Date time) { mStartTime = time; }

    public Date getEndTime() { return mEndTime; }
    public void setEndTime(Date time) { mEndTime = time; }

    public boolean isAuction() { return auction; }
    public void setAuction(boolean auction) { this.auction = auction; }

    public boolean isBuyItNow() { return buyItNow; }
    public void setBuyItNow(boolean buyItNow) { this.buyItNow = buyItNow; }

    @Override
    public int compareTo(@NonNull EbayListing another){ return(another.mStartTime.compareTo(this.mStartTime)); }

}
