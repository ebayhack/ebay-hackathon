package com.evatar.ebayapi;

import android.text.TextUtils;
import android.util.Log;

import com.evatar.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class EbayRequest {

    private static final String TAG = "EbayRequest";
    private static final int STATUS_CODE_ERROR_MIN = 400;

    private String maApiCallName;

    private EbayRequest() {

    }

    public EbayRequest(String apiCallName) {
        maApiCallName = apiCallName;
    }

    private String getRequestURL() {
        CharSequence requestURL = TextUtils.expandTemplate(EbayAPI.getApplicationContext().getResources().getString(R.string.ebay_request_template),
                EbayAPI.getBaseRequestUrl(),
                maApiCallName,
                EbayAPI.getAppId(),
                getCallParameterString());
        return(requestURL.toString());
    }

    public abstract String getCallParameterString();

    public final String invokeResponse() throws Exception {

        String result = null;

        String request = getRequestURL();

        Log.d("URL", request);

//        request = request.replaceAll(" ", "%20");

        URL url = new URL(request);
        HttpURLConnection httpClient = (HttpURLConnection)url.openConnection();
        httpClient.setRequestMethod("GET");
        httpClient.setRequestProperty("Content-Type", "application/json");
        long length = url.toString().length();
//        httpClient.setRequestProperty("Content-Length", String.valueOf(length));
        httpClient.connect();
        InputStream response;
        int status = httpClient.getResponseCode();

        if(status >= STATUS_CODE_ERROR_MIN) {
            response = httpClient.getErrorStream();
            Log.d(TAG, "Response error code " + status);
        } else {
            response = httpClient.getInputStream();
            Log.d(TAG, "Response code " + status);
        }

        if(response != null){
            BufferedReader reader = new BufferedReader(new InputStreamReader(response));
            StringBuffer temp = new StringBuffer();
            String currentLine;

            while((currentLine = reader.readLine()) != null){
                temp.append(currentLine);
            }

            result=temp.toString();
            response.close();
        }

        httpClient.disconnect();

        return(result);

    }

}
