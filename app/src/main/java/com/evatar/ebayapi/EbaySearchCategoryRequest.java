package com.evatar.ebayapi;

/**
 * Created by miki on 11/27/15.
 */
public class EbaySearchCategoryRequest extends EbayRequest {
    private int mCategoryId;

    public static final int NUM_ENTRIES_DEFAULT = 25;

    private int numEntries, numPage;

    public EbaySearchCategoryRequest(int categoryId, int numEntries) {
        super("findItemsByCategory");
        this.numEntries = numEntries;
        mCategoryId = categoryId;
        numPage = 1;
    }

    public EbaySearchCategoryRequest(int categoryId) {
        this(categoryId, NUM_ENTRIES_DEFAULT);
    }

    public void setPageNumber(int num) {
        numPage = num;
    }

    public int getPageNumber() { return numPage; }

    public void incrementPage() { numPage++; }

    @Override
    public String getCallParameterString() {
        StringBuilder builder = new StringBuilder();
        builder.append("categoryId=" + String.valueOf(mCategoryId)).append("&")
                .append("paginationInput.entriesPerPage=" + String.valueOf(numEntries)).append("&")
                .append("paginationInput.pageNumber=" + String.valueOf(numPage));
        return  builder.toString();
    }
}
