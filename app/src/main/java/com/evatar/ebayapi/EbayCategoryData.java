package com.evatar.ebayapi;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by miki on 12/3/15.
 */
public class EbayCategoryData {
    public static final Map<String, Integer> categoryDataMap = new LinkedHashMap<>();

    static {
//        categoryDataMap.put("Dancewear", 112425);
//        categoryDataMap.put("Kids", 171146);
//        categoryDataMap.put("Men's Accessories", 4250);
//        categoryDataMap.put("Men's Clothing", 1059);
        categoryDataMap.put("Men's T-shirts:torso;normal",15687);
        categoryDataMap.put("Men's Shoes:shoes;normal", 24087);
        categoryDataMap.put("Men's Casual:torso;normal", 57990);
        categoryDataMap.put("Men's Coats:torso;big", 57988);
        categoryDataMap.put("Men's Formal:torso;formal", 57991);
        categoryDataMap.put("Men's Jeans:legs;normal", 11483);
        categoryDataMap.put("Men's Shirts:torso;normal", 15689);

        categoryDataMap.put("Women's T-shirts:torso;normal",63869);
//        categoryDataMap.put("Women's Shoes:shoes;normal", 24087);
//        categoryDataMap.put("Women's Casual:torso;normal", 57990);
        categoryDataMap.put("Women's Coats:torso;big", 63862);
        categoryDataMap.put("Women's Suits:torso;formal", 63865);
        categoryDataMap.put("Women's Jeans:legs;normal", 11554);
        categoryDataMap.put("Women's Shirts:torso;normal", 53159);
//        categoryDataMap.put("Vintage", 175759);
//        categoryDataMap.put("Wedding & Formal", 3259);
//        categoryDataMap.put("Women's Accessories", 4251);
//        categoryDataMap.put("Women's Handbag", 169291);
//        categoryDataMap.put("Women's Clothing", 15724);
//        categoryDataMap.put("Traditional Clothes", 155240);
//        categoryDataMap.put("Other", 312);
    }
}
