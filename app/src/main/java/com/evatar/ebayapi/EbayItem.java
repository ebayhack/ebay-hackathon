package com.evatar.ebayapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by miki on 12/4/15.
 */
public class EbayItem implements Comparable<EbayItem>{
    private String itemId;
    private String ebayURL;
    private String listingType;
    private String location;
    private String[] pictureUrls;
    private String categoryId;
    private String categoryFullName;
    private int bidCount;
    private double price;
    private String currency;
    private String listingStatus;
    private String title;
    private String countrySymbol;
    private boolean autoPay;
    private String quantityHint;
    private int quantityThreshold, quantity;
    private LinkedHashMap<String, String[]> itemSpecifics;

    private Date endTime;

    public EbayItem() {

    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getEbayURL() {
        return ebayURL;
    }

    public void setEbayURL(String ebayURL) {
        this.ebayURL = ebayURL;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(String[] pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryFullName() {
        return categoryFullName;
    }

    public void setCategoryFullName(String categoryFullName) {
        this.categoryFullName = categoryFullName;
    }

    public int getBidCount() {
        return bidCount;
    }

    public void setBidCount(int bidCount) {
        this.bidCount = bidCount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountrySymbol() {
        return countrySymbol;
    }

    public void setCountrySymbol(String countrySymbol) {
        this.countrySymbol = countrySymbol;
    }

    public boolean isAutoPay() {
        return autoPay;
    }

    public void setAutoPay(boolean autoPay) {
        this.autoPay = autoPay;
    }

    public String getQuantityHint() {
        return quantityHint;
    }

    public void setQuantityHint(String quantityHint) {
        this.quantityHint = quantityHint;
    }

    public int getQuantityThreshold() {
        return quantityThreshold;
    }

    public void setQuantityThreshold(int quantityThreshold) {
        this.quantityThreshold = quantityThreshold;
    }

    public int getQuantity() { return quantity; }

    public void setQuantity(int quantity) { this.quantity = quantity; }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public LinkedHashMap<String, String[]> getItemSpecifics() {
        return itemSpecifics;
    }

    public void setItemSpecifics(LinkedHashMap<String, String[]> itemSpecifics) {
        this.itemSpecifics = itemSpecifics;
    }

    public static EbayItem fromJson(JSONObject json) {
        EbayItem item = new EbayItem();
        try {
            String jsonAck = json.getString("Ack");
            if(jsonAck.equals("Failure")) {
                //Means that request didn't give any item. Id corrupted or wrong request
                Log.e("EbayItem JSON", "Ack returned failure: " + json.toString());
                return null; //TODO Handle
            }
        } catch (JSONException ex) {
            ex.printStackTrace(); //TODO Handle
            return null;
        }

        JSONObject itemRoot;

        String itemId = "", url = "", type = "", location = "", categoryId = "", categoryName = "";
        String  currencySymbol = "", listingStatus = "", title = "", countrySymbol = "", quantityHint = "";
        int bidCount = -1, quantityThreshold = -1, quantity = 0;
        double price = -1;
        Date endTime;
        boolean autoPay = false;
        String[] pictureUrls;
        try {
            itemRoot = json.getJSONObject("Item");
            itemId = itemRoot.getString("ItemID");
            url = itemRoot.getString("ViewItemURLForNaturalSearch");
            type = itemRoot.getString("ListingType");
            JSONObject priceObj = itemRoot.getJSONObject("ConvertedCurrentPrice");
            price = priceObj.getDouble("Value");
            currencySymbol = priceObj.getString("CurrencyID");
        } catch (JSONException ex) {
            ex.printStackTrace(); //TODO Handle
            return null;
        }

        try { location = itemRoot.getString("Location"); } catch(JSONException e) { e.printStackTrace(); }
        try {
            JSONArray pictureGallery = itemRoot.getJSONArray("PictureURL");
            pictureUrls = new String[pictureGallery.length()];
            for(int i = 0; i < pictureGallery.length(); i++) {
                pictureUrls[i] = pictureGallery.getString(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            pictureUrls = null;
        }

        try { categoryId = itemRoot.getString("PrimaryCategoryID"); } catch (JSONException e) { e.printStackTrace(); }
        try { categoryName = itemRoot.getString("PrimaryCategoryName"); } catch (JSONException e) { e.printStackTrace(); }
        try { listingStatus = itemRoot.getString("ListingStatus"); } catch (JSONException e) { e.printStackTrace(); }
        try { title = itemRoot.getString("Title"); } catch (JSONException e) { e.printStackTrace(); }
        try { countrySymbol = itemRoot.getString("Country"); } catch (JSONException e) { e.printStackTrace(); }
        try { autoPay = itemRoot.getBoolean("AutoPay"); } catch (JSONException e) { e.printStackTrace(); }
        try { quantityHint = itemRoot.getString("QuantityAvailableHint"); } catch (JSONException e) { e.printStackTrace(); }
        try { quantityThreshold = itemRoot.getInt("QuantityThreshold"); } catch (JSONException e) { e.printStackTrace(); }
        try { quantity = itemRoot.getInt("Quantity") - itemRoot.getInt("QuantitySold"); } catch (JSONException e) { e.printStackTrace(); }

        LinkedHashMap<String, String[]> itemSpecifics = new LinkedHashMap<>();
        try {
            JSONObject specificsObj = itemRoot.getJSONObject("ItemSpecifics");
            JSONArray specificsNameValueArray = specificsObj.getJSONArray("NameValueList");
            for(int i = 0; i <specificsNameValueArray.length(); i++) {
                JSONObject pair = specificsNameValueArray.getJSONObject(i);
                String name = null;
                JSONArray valuesJson;
                try {
                    name = pair.getString("Name");
                    valuesJson = pair.getJSONArray("Value");
                } catch (JSONException ex) {
                    continue;
                }
                String[] values = new String[valuesJson.length()];
                for(int j = 0 ; j < valuesJson.length(); j++) values[j] = valuesJson.getString(j);
                itemSpecifics.put(name, values);
            }
        } catch(JSONException ex) {

        }

        item.setAutoPay(autoPay);
        item.setBidCount(bidCount);
        item.setCategoryFullName(categoryName);
        item.setCategoryId(categoryId);
        item.setCountrySymbol(countrySymbol);
        item.setCurrency(currencySymbol);
        item.setEbayURL(url);
        //TODO Set date
        item.setItemId(itemId);
        item.setListingStatus(listingStatus);
        item.setListingType(type);
        item.setLocation(location);
        item.setPictureUrls(pictureUrls);
        item.setPrice(price);
        item.setQuantityHint(quantityHint);
        item.setQuantityThreshold(quantityThreshold);
        item.setQuantity(quantity);
        item.setTitle(title);
        item.setItemSpecifics(itemSpecifics);

        return item;
    }

    @Override
    public int compareTo(EbayItem another) {
        return title.compareTo(another.title);
    }
}
