package com.evatar.ebayapi;

public class EbaySearchRequest extends EbayRequest {

    private String mKeywords;

    public static final int NUM_ENTRIES_DEFAULT = 40;

    private int numEntries, numPage;

    public EbaySearchRequest(String mKeywords) {
        this(mKeywords, NUM_ENTRIES_DEFAULT);
    }

    public EbaySearchRequest(String mKeywords, int numEntries) {
        super("findItemsByKeywords");
        this.mKeywords = mKeywords;
        this.numEntries = numEntries;
        this.numPage = 1;
    }

    public void incrementPage() { numPage++; }

    @Override
    public String getCallParameterString() {
        StringBuilder builder = new StringBuilder();
        builder.append("keywords=" + mKeywords.replaceAll(" ", "%20")).append("&")
                .append("paginationInput.entriesPerPage=" + String.valueOf(numEntries)).append("&")
                .append("paginationInput.pageNumber=" + String.valueOf(numPage));
        return  builder.toString();
    }
}