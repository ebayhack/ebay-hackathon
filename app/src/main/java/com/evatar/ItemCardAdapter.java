package com.evatar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.evatar.parse.User;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by miki on 12/13/15.
 */
public abstract class ItemCardAdapter<T> extends RecyclerView.Adapter<ItemCardAdapter.ViewHolder>{

    private static final int TYPE_ITEM = 1;

    private Context mContext;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

//    private HeaderViewHolder mHeaderViewHolder;

    private int lastPosition = -1;

    private List<T> mData;
    private List<Bitmap> mImageData;

    protected User mUser;

    private ItemCardAdapter() {}

    public ItemCardAdapter(Context ctx,final List<T> listingData, User user) {
        mUser = user;
        mContext = ctx;
        if(listingData != null) {
            mData = listingData;
        } else {
            mData = new ArrayList<>();
        }
        mImageData = new ArrayList<>(mData.size());
        for(int i = 0; i < mData.size(); i ++) mImageData.add(null);
    }

    public void addAll(List<T> listings) {
        int size = getItemCount();
        mData.addAll(listings);
        notifyItemRangeInserted(size, mData.size() - 1);
    }

    public void addItem(T item) {
        mData.add(item);
        notifyItemInserted(mData.size() - 1);
    }

    public void removeItem(T item) {
        int pos = 0;
        while(!mData.get(pos).equals(item)) pos++;
        if(mData.remove(item)) {
            notifyItemRemoved(pos);
        }
    }

    public Context getContext() { return mContext; }

    public abstract String getImageUrl(List<T> items, int position);

    public abstract Bitmap getImage(List<T> items, int position);

    public abstract void onBindCardData(@NonNull ItemCardAdapter.ViewHolder holder, final int position);

    @SuppressLint("NewApi") @Override
    public ItemCardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        if (type == TYPE_ITEM) {
            View itemView = inflater.inflate(R.layout.card_item, viewGroup, false);
            return new ViewHolder(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + type + " + make sure you're using types correctly");
    }

     @Override
     public void onBindViewHolder(final ItemCardAdapter.ViewHolder holder, final int position) {

        if (holder != null) {
            setAnimation(holder.rootView, position);
            String imageUrl = getImageUrl(mData, position);
            if(imageUrl != null) {
                imageUrl = imageUrl.replaceAll("\\\\", "");
                if (mImageData.get(position) == null) {
                    Picasso.with(mContext).load(imageUrl).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mImageData.add(position, bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                    Picasso.with(mContext).load(imageUrl).error(R.mipmap.ic_launcher).into(holder.productImage);
                } else {
                    holder.productImage.setImageBitmap(mImageData.get(position));
                }
            } else if(getImage(mData, position) != null){
                new AsyncTask<Void, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Void... params) {
                        return getImage(mData, position);
                    }

                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        holder.productImage.setImageBitmap(bitmap);
                    }
                }.execute(null, null, null);
            } else {
                Log.e("CardAdapter", "No image for item " + position);
            }

            onBindCardData(holder, position);


//            vh.productPrice.setText(getItem(position).getCurrentPrice());
//            vh.setClickListener(new ItemClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent viewItem = new Intent(mContext, ViewEbayItemActivity.class);
//                    viewItem.putExtra("itemId", getItem(position).getId());
//                    mContext.startActivity(viewItem);
//                }
//            });
//            vh.productName.setText(getItem(position).getTitle());

        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.recyclerview_item_enter);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public T getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.card_product_image_view)
        public ImageView productImage;
        @Bind(R.id.card_product_name_textview)
        public TextView productName;
        @Bind(R.id.card_product_brand_textview)
        public TextView productBrand;
        @Bind(R.id.card_product_price_textview)
        public TextView productPrice;
        @Bind(R.id.card_product)
        public CardView rootView;
        @Bind(R.id.item_currency_image)
        public ImageView itemCurrencyImage;

        private ItemClickListener mListener;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) mListener.onClick(v);
                }
            });
        }

        public void setClickListener(ItemClickListener clickListener) {
            mListener = clickListener;
        }
    }

    public interface ItemClickListener {
        void onClick(View view);
    }
}

