package com.evatar;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evatar.avatarview.AvatarView;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder>{

    private List<AvatarBundle> mData;
    private Activity mContext;

    private int lastPosition = -1;

    private FeedAdapter() {

    }

    public FeedAdapter(Activity c, List<AvatarBundle> data) {
        mContext = c;
        mData = data;
        Collections.sort(mData);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.feed_list_item, parent, false);
        ViewHolder holder = new ViewHolder(v);
        holder.setListener(new ItemListener() {
            @Override
            public void onClick(View viewClicked) {
                Snackbar.make(viewClicked, "Item Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        return holder;
    }

    public void addAll(List<AvatarBundle> items) {
//        mData.clear();
        mData.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setAnimation(holder.rootView, position);
        AvatarBundle bundle = getItem(position);
        holder.mUsernameTv.setText(bundle.getOwnerName());
//        holder.mAvatarView.setTorsoBitmap(bundle.getItem(AvatarView.AVATAR_TORSO), false);
//        holder.mAvatarView.setLegsBitmap(bundle.getItem(AvatarView.AVATAR_LEGS), false);
//        holder.mAvatarView.setShoesBitmap(bundle.getItem(AvatarView.AVATAR_SHOES), false);
        Utils.initAvatar(mContext, holder.mAvatarView, bundle.getAvatar());
        holder.mDateTv.setText(Utils.parseDate(bundle.getDateCreated()));
        Picasso.with(mContext).load(bundle.getPhotoUrl()).resize(144,144).into(holder.profilePic);
//        holder.mDateTv.setText(bundle.getDateCreated().toString());
    }


    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.recyclerview_item_enter);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public AvatarBundle getItem(int position) { return mData.get(position); }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        private ItemListener mListener;
        public AvatarView mAvatarView;
        public TextView mUsernameTv, mDateTv;
        public CircleImageView profilePic;

        public LinearLayout rootView;

        public ViewHolder(View v) {
            super(v);
            mAvatarView = (AvatarView)v.findViewById(R.id.feed_list_item_avatar_view);
            mUsernameTv = (TextView)v.findViewById(R.id.feed_list_item_username);
            mDateTv = (TextView)v.findViewById(R.id.feed_list_item_date);
            profilePic = (CircleImageView)v.findViewById(R.id.feed_list_item_profile_image);
            rootView = (LinearLayout)v.findViewById(R.id.feed_list_item_root);
            rootView.setClickable(true);
            rootView.setOnClickListener(this);
        }

        public void setListener(ItemListener listener) { mListener = listener; }

        @Override
        public void onClick(View v) {
            if(mListener != null) mListener.onClick(v);
        }
    }

    interface ItemListener {
        void onClick(View viewClicked);
    }
}
