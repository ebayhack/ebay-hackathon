package com.evatar;

import android.app.Application;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;

import com.evatar.ebayapi.EbayAPI;
import com.evatar.parse.Avatar;
import com.evatar.parse.Item;
import com.evatar.parse.User;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by miki on 11/13/15.
 */
public class MainApplication extends Application{

    private static final String PARSE_AUTH_1 = "h5tdHKe0GjfIZ1VBV94kkDqa0b7uT3YQ4v5xElXP";
    private static final String PARSE_AUTH_2 = "Op1ZeCVhLvtyLTCZe4lpQCSLbdRVgDq2jGIKqyx6";


    private User mUser;

    @Override
    public void onCreate() {
        super.onCreate();
        initParse();
        initEbay();
        PreferenceUtils.init(getApplicationContext());

        try {
            mUser = ParseQuery.getQuery(User.class).get(PreferenceUtils.readString("userObjectId"));
        } catch(ParseException ex) {
            ex.printStackTrace(); //TOOD Handle
            mUser = null;
        }
    }

    private void initParse() {
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Avatar.class);
        ParseObject.registerSubclass(User.class);
        ParseObject.registerSubclass(Item.class);
        Parse.initialize(this, PARSE_AUTH_1, PARSE_AUTH_2);
        ParseFacebookUtils.initialize(getApplicationContext());
    }

    private void initEbay() {
        EbayAPI.initialize(getApplicationContext(),
                EbayAPI.PRODUCTION);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Nullable
    public User getCurrentUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
        if(user != null) {
            PreferenceUtils.putString("userObjectId", user.getObjectId());
        }
    }
}
