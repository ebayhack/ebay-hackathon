package com.evatar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evatar.ebayapi.EbayAPI;
import com.evatar.ebayapi.EbayCategoryData;
import com.evatar.ebayapi.EbayItem;
import com.evatar.ebayapi.EbayUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ViewEbayItemActivity extends BaseActivity implements View.OnClickListener{

    private static final String API_SANDBOX = "http://open.api.sandbox.ebay.com/shopping";
    private static final String API_PRODUCTION = "http://open.api.ebay.com/shopping";

    private static final String URL_FORMAT = "?callname=GetSingleItem&responseencoding=JSON&appid=^1&siteid=0&version=847&ItemID=^2&IncludeSelector=ItemSpecifics,Details";
    private static final String APP_ID = "MikiMint-6aec-4342-8cdf-994b0782955a";
    private static final String ITEM_ID = "321915191231";

    private static final String TAG = ViewEbayItemActivity.class.getSimpleName();

    private EbayItem mEbayItem;

    String metaTag, metaType;
    int metaColor;

    @Bind(R.id.collapsing_toolbar_backdrop_pager)
    ViewPager mBackdropPager;
//    @Bind(R.id.view_item_tab_layout)
//    TabLayout mTabLayout;
    @Bind(R.id.view_item_toolbar)
    Toolbar mToolbar;
    @Bind(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout mCollapsingToolbar;

    @Bind(R.id.view_ebay_item_btn_buy_now)
    Button mBuyNowButton;
    @Bind(R.id.view_ebay_item_btn_add_to_avatar)
    Button mAddToAvatarButton;

    @Bind(R.id.card_item_extra_info_linearlayout)
    LinearLayout mItemExtraInfoLayout;
    @Bind(R.id.card_item_shipping_info_linearlayout)
    LinearLayout mItemShippingInfoLayout;
    @Bind(R.id.card_item_info_linearlayout)
    LinearLayout mItemInfoLayout;

    private String itemId;

    private int paletteColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_ebay_item);

        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textColorPrimary));
        mCollapsingToolbar.setTitle("");
        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);

        mBuyNowButton.setClickable(false);
        mAddToAvatarButton.setClickable(false);

        paletteColor = getIntent().getIntExtra("paletteColor", getResources().getColor(R.color.colorPrimary));
        itemId = getIntent().getStringExtra("itemId");
        if(itemId == null) {
            finish();
        }
        Log.d("ItemId post remove", itemId);
        itemId = itemId.substring(2, itemId.length() - 2);

        final String request = API_PRODUCTION + TextUtils.expandTemplate(URL_FORMAT, EbayAPI.getAppId(), itemId).toString();
        Log.d(TAG, request);
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    return retreiveJsonForItem(request);
                } catch (Exception ex) {
                    ex.printStackTrace(); //TODO Handle
                }
                return null;
            }

            @Override protected void onPostExecute(String s) { setupOnPostLoad(s); }
        }.execute(null, null, null);

        mBuyNowButton.setOnClickListener(this);
        mAddToAvatarButton.setOnClickListener(this);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        ViewUtils.overrideFonts(this, findViewById(R.id.activity_main_content), "fonts/gabriel.otf");
    }

    private void setAdapter(LinearLayout layout, BaseAdapter adapter) {
        for(int i = 0 ; i < adapter.getCount();i++) {
            layout.addView(adapter.getView(i, null, null));
        }
    }

    private void setupOnPostLoad(String response) {
        if(response == null) {
            return;
        }
        try {
            JSONObject obj = new JSONObject(response);
            mEbayItem = EbayItem.fromJson(obj);

            mBuyNowButton.setClickable(true);
            mAddToAvatarButton.setClickable(true);

            Map<String, String> itemInfo = new HashMap<>();
            itemInfo.put("Price", EbayUtils.formatPrice(mEbayItem.getCurrency(), mEbayItem.getPrice()));
            String categoryName = mEbayItem.getCategoryFullName();
            String id = mEbayItem.getCategoryId();
            for(Map.Entry<String, Integer> entry : EbayCategoryData.categoryDataMap.entrySet()) {
//                String name = entry.getKey().split(":")[0];
                int entryId = entry.getValue();
                String metaTag = entry.getKey().split(":")[1].split(";")[0];
                String metaType = entry.getKey().split(":")[1].split(";")[1];

                if(String.valueOf(entryId).equals(id)) {
                    this.metaTag = metaTag;
                    this.metaType = metaType;
                }
            }
            itemInfo.put("Category", categoryName.replaceAll(":", " > "));
            itemInfo.put("Location", mEbayItem.getLocation());
            itemInfo.put("AutoPay", mEbayItem.isAutoPay() ? "Yes" : "No");
            String quantityString = mEbayItem.getQuantityThreshold() == -1 ? mEbayItem.getQuantity() + " Pieces" :
                    mEbayItem.getQuantityHint() + " " + String.valueOf(mEbayItem.getQuantityThreshold());
            itemInfo.put("Available", quantityString);
            setAdapter(mItemInfoLayout, new ItemInfoListAdapter(itemInfo));

            Map<String, String> itemSpecifics = new HashMap<>();
            for(Map.Entry<String, String[]> entry : mEbayItem.getItemSpecifics().entrySet()) {
                StringBuilder concat = new StringBuilder();
                for(String s1 : entry.getValue()) {
                    concat.append(s1 == null ? " - " : s1).append(",");
                }
                concat.deleteCharAt(concat.length() - 1);
                itemSpecifics.put(entry.getKey(), concat.toString());
            }
            setAdapter(mItemExtraInfoLayout, new ItemInfoListAdapter(itemSpecifics));

            mCollapsingToolbar.setTitle(mEbayItem.getTitle());
            String[] brands = mEbayItem.getItemSpecifics().get("Brand");
//            if(brands != null && brands.length > 0)
//                Toast.makeText(ViewEbayItemActivity.this, brands[0], Toast.LENGTH_SHORT).show();
            mBackdropPager.setAdapter(new ImagePagerAdapter(ViewEbayItemActivity.this, mEbayItem.getPictureUrls()));
            Picasso.with(ViewEbayItemActivity.this).load(mEbayItem.getPictureUrls()[0]).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    metaColor = paletteColor;
                    mCollapsingToolbar.setContentScrimColor(paletteColor);
                    mCollapsingToolbar.setTitle(mEbayItem.getTitle());
//                    Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//                        @Override
//                        public void onGenerated(Palette palette) {
//                            metaColor = palette.getVibrantColor(getResources().getColor(R.color.colorPrimary));
//                            mCollapsingToolbar.setContentScrimColor(metaColor);
//                        }
//                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        } catch (JSONException ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_ebay_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_ebay_item_btn_buy_now:
//                ApiAccount apiAccount =new ApiAccount();
//                apiAccount.setApplication(EbayAPI.getAppId());
//                ApiCredential apiCredential = new ApiCredential();
//                apiCredential.setApiAccount(apiAccount);
//                ApiContext apiContext = new ApiContext();
//                apiContext.setApiCredential(apiCredential);
//                apiContext.setApiServerUrl("https://api.sandbox.ebay.com/wsapi");
//                PlaceOfferCall placeOfferCall = new PlaceOfferCall(apiContext);
//                OfferType type = new OfferType();
//                type.setAction(BidActionCodeType.PURCHASE);
//                placeOfferCall.setOffer(type);
//                placeOfferCall.setItemID(itemId);
//                SellingStatusType response;
//                try {
//                    response = placeOfferCall.placeOffer();
//                    //response.getListingStatus().value();
//                } catch (ApiException ex) {
//                    ex.printStackTrace();
//                } catch (SdkException ex2) {
//                    ex2.printStackTrace();
//                } catch(Exception ex3) {
//                    ex3.printStackTrace();
//                }

                String url = "http://www.ebay.com/itm/" + itemId;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.view_ebay_item_btn_add_to_avatar:
                if(metaTag == null) {
                    Toast.makeText(ViewEbayItemActivity.this, "This is not a compatible item", Toast.LENGTH_SHORT).show();
                }
                PreferenceUtils.putString(metaTag, metaColor + ":" + metaTag + ":" + mEbayItem.getItemId());
                onBackPressed();
                break;
        }
    }

    private String retreiveJsonForItem(String request) throws Exception {

        String result = null;

        URL url = new URL(request);
        HttpURLConnection httpClient = (HttpURLConnection)url.openConnection();
        long length = url.toString().length();
        httpClient.connect();
        InputStream response;
        int status = httpClient.getResponseCode();

        if(status >= 400) {
            response = httpClient.getErrorStream();
            Log.d(TAG, "Response error code " + status);
        } else {
            response = httpClient.getInputStream();
            Log.d(TAG, "Response code " + status);
        }

        if(response != null){
            BufferedReader reader = new BufferedReader(new InputStreamReader(response));
            StringBuffer temp = new StringBuffer();
            String currentLine;

            while((currentLine = reader.readLine()) != null){
                temp.append(currentLine);
            }

            result=temp.toString();
            response.close();
        }

        httpClient.disconnect();

        return(result);

    }

    class ItemInfoListAdapter extends BaseAdapter {

        private Map<String, String> mData;

        public ItemInfoListAdapter(Map<String, String> data) {
            mData = data;
        }

        @Override public int getCount() { return mData.size(); }

        @Override
        public String getItem(int position) {
            Iterator<String> it = mData.keySet().iterator();
            String val = it.next();
            for(int i = 0; i < position; i++) {
                val = it.next();
            }
            return val;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = View.inflate(ViewEbayItemActivity.this, R.layout.ebay_item_info_item, null);
            }
            TextView headerTv = (TextView)convertView.findViewById(R.id.ebay_info_item_header);
            TextView contentTv = (TextView)convertView.findViewById(R.id.ebay_info_item_text);
            String header = getItem(position);
            headerTv.setText(header);
            contentTv.setText(mData.get(header));

            return convertView;
        }
    }

    class ImagePagerAdapter extends FragmentPagerAdapter {

        private String[] urls;
        private Fragment[] frags;

        public ImagePagerAdapter(BaseActivity a, String[] urls) {
            super(a.getSupportFragmentManager());
            this.urls = urls;
            frags = new Fragment[urls.length];
            int i = 0;
            for(final String url : urls) {
                frags[i] = new Fragment() {
                    @Nullable @Override
                    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                        View view = inflater.inflate(R.layout.backdrop_image, container, false);

                        ImageView image = (ImageView)view.findViewById(R.id.collapsing_toolbar_backdrop_image);
                        Picasso.with(getContext()).load(url).into(image);

                        return view;
                    }
                };
                i++;
            }
        }

        @Override
        public Fragment getItem(int position) {
            return frags[position];
        }

        @Override
        public int getCount() {
            return urls.length;
        }
    }
}
