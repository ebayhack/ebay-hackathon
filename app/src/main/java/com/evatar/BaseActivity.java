package com.evatar;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * <p> This is a base class for activities that defines a set of methods and modifications to properly customize
 * all activites in the app with material design guidelines and give them the same overall look and feel for a unified
 * user experience on all app sections </p>
 */
public abstract class BaseActivity extends AppCompatActivity {

	private static final int ANIM_NEXT_ACTIVITY_IN = 0;
	private static final int ANIM_CURRENT_ACTIVITY_OUT = 0;
	private static final int ANIM_NEXT_ACTIVITY_OUT = 0;
	private static final int ANIM_CURRENT_ACTIVITY_IN = 0;

	/**
	 * The toolbar used as the action bar in all activities that extends {@link BaseActivity}
	 */
	private Toolbar mToolbar;

	private TabLayout mTabLayout;

	/** The toolbar wrapper layout */
	private RelativeLayout mToolbarWrapperLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		initAppCompatDesign();
		initViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	/**
	 * Helper method to initiate all the material design components with backwards compatibility
	 */
	protected void initAppCompatDesign() {
		setStatusBarColor(R.color.colorPrimaryDark);
        mToolbarWrapperLayout = (RelativeLayout) findViewById(R.id.toolbar);
        if (mToolbarWrapperLayout != null) {
            mToolbar = (Toolbar) mToolbarWrapperLayout.findViewById(R.id.toolbarInner);
            setSupportActionBar(mToolbar);
			mTabLayout = (TabLayout)mToolbarWrapperLayout.findViewById(R.id.toolbar_tab_layout);
        }
	}

	public void initAppCompatDesign(AppBarLayout appBarLayout) {
//		setStatusBarColor(R.color.colorPrimaryDark);
//		mToolbarWrapperLayout = (RelativeLayout) findViewById(R.id.toolbar);
		if (appBarLayout != null) {
			mToolbar = (Toolbar) appBarLayout.findViewById(R.id.toolbarInner);
			setSupportActionBar(mToolbar);
			mTabLayout = (TabLayout)appBarLayout.findViewById(R.id.toolbar_tab_layout);
		}
	}

	public void setupTabsWithViewPager(ViewPager pager) {
		if(mTabLayout != null) {
			PagerAdapter adapter = pager.getAdapter();
			for(int i = 0; i < adapter.getCount();i++) {
				mTabLayout.addTab(mTabLayout.newTab().setText(adapter.getPageTitle(i)));
			}
			mTabLayout.setupWithViewPager(pager);
		}
	}

	public void setToolbar(Toolbar t) {
		mToolbar = t;
		setSupportActionBar(t);
	}

	public void setTabLayout(TabLayout tabLayout) {
		mTabLayout = tabLayout;
	}

	public TabLayout getTabLayout() {
		return mTabLayout;
	}

	public void setTabLayoutVisibility(int visibility) {
		if(mTabLayout != null) {
			mTabLayout.setVisibility(visibility);
		}
	}

	/**
	 * Sets the status bar color for devices that run version {@value Build.VERSION_CODES#LOLLIPOP} and up
	 * @param colorResId The resource id the color
	 */
	public void setStatusBarColor(int colorResId) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(colorResId));
		}
	}

	@Override
	public void startActivity(Intent intent) {
		super.startActivity(intent);
		if(ANIM_NEXT_ACTIVITY_IN != 0 && ANIM_CURRENT_ACTIVITY_OUT != 0) {
			overridePendingTransition(ANIM_NEXT_ACTIVITY_IN, ANIM_CURRENT_ACTIVITY_OUT);
		}
	}

	/**
	 * gets this activity's toolbar, or null if one isn't defined
	 * @return The activity toolbar that is also used as the action bar
	 */
    @Nullable
	public Toolbar getToolbar() { return mToolbar; }

	@Override
	public void finish() {
		super.finish();
		if(ANIM_CURRENT_ACTIVITY_IN != 0 && ANIM_NEXT_ACTIVITY_OUT != 0) {
			overridePendingTransition(ANIM_CURRENT_ACTIVITY_IN, ANIM_NEXT_ACTIVITY_OUT);
		}
	}

    /**
     * Enable or disable the up button on the toolbar with some customization
     * @param enabled Set the up button to be enabled or disabled
     * @param drawableResId A resource id for the button {@link Drawable}
     * @param drawableColorFilter A color resource to use as a filter for the drawable
     */
    public void setDisplayHomeAsUpEnabled(boolean enabled, int drawableResId, int drawableColorFilter) {
        final Drawable upArrow = getResources().getDrawable(drawableResId);
        if(upArrow != null) {
            upArrow.setColorFilter(getResources().getColor(drawableColorFilter), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(enabled);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enabled);
        }
    }

    /**
     * Enable or disable the up button on the toolbar with some customization.
     * @param enabled Set the up button to be enabled or disabled
     * @param drawableResId A resource id for the button {@link Drawable}
     */
    public void setDisplayHomeAsUpEnabled(boolean enabled, int drawableResId) {
        setDisplayHomeAsUpEnabled(enabled, drawableResId, R.color.md_white_1000);
    }

    private Snackbar getSnackbar(String s) {
        View root = findViewById(android.R.id.content);
        if(root != null) {
            return Snackbar.make(root, s, Snackbar.LENGTH_LONG);
        } else return null;
    }

    /**
     * Shows a simple snackbar for this activity with one or less actions
     * @param message The message to be shown
     * @param action The action name
     * @param listener The action {@link View.OnClickListener}
     */
	public void snackbar(String message, String action, View.OnClickListener listener) {
        Snackbar snackbar = getSnackbar(message);
        if(snackbar != null) {
            snackbar.setAction(action, listener).show();
        } else {
			//TODO Replace with custom dialog
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		}
	} //TODO Add support for coordinator layout

    /**
     * @see #snackbar(String, String, View.OnClickListener)
     * @param message
     */
    public void snackbar(String message) {
        Snackbar s = getSnackbar(message);
        if(s != null) s.show();
        else Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

	/**
	 * Initiate all the views in this activity, right after calling setContentView
	 */
	protected abstract void initViews();

}
