package com.evatar.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.evatar.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class AvatarView extends View {



    private BodyComponent mBodyComponentFirst;
    private BodyComponent torso, legs, shoes;

    public static final String AVATAR_HEAD = "head";
    public static final String AVATAR_TORSO = "torso";
    public static final String AVATAR_LEGS = "legs";
    public static final String AVATAR_SHOES = "shoes";

    private Paint mPaint;

    public AvatarView(Context context) {
        super(context);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
//        mBodyComponentFirst = new ArrayList<>();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(getResources().getColor(R.color.md_white_1000));
        initFromAssets();
    }

    public void initFromAssets() {
        initFromAssets("hair_2", "torso_1", "legs_1", "shoes_1");
    }

    public void setShoes(String name) {
        shoes.addWearable(new WearableComponent(getBitmapFromAsset(getContext(), name + ".png", AVATAR_SHOES), new CanvasPoint(0, 0)));
        invalidate();
    }

    public void setTorso(String name) {
        torso.addWearable(new WearableComponent(getBitmapFromAsset(getContext(), name + ".png", AVATAR_TORSO), new CanvasPoint(0, 0)));
        invalidate();
    }

    public void setLegs(String name) {
        legs.addWearable(new WearableComponent(getBitmapFromAsset(getContext(), name + ".png", AVATAR_LEGS), new CanvasPoint(0, 0)));
        invalidate();


    }

    public void setHair(String name) {
        mBodyComponentFirst.addWearable(new WearableComponent(getBitmapFromAsset(getContext(), name + ".png", AVATAR_HEAD), new CanvasPoint(0, 0)));
    }

    public void initFromAssets(String name, int componentIndex) {
        BodyComponent comp = mBodyComponentFirst;
        for(int i = 0; i < componentIndex - 1; i++) {
            if(comp.getNext() == null) {
                return;
            }
            comp = comp.getNext();
        }
        BodyComponent compNext = comp.getNext();
        comp = new BodyComponent(getBitmapFromAsset(getContext(), name + ".png", ""), new CanvasPoint(0,0));
        comp.setNext(compNext);
    }

    public void initFromAssets(String hairName, String torsoName, String legsName, String shoesName) {
        Context ctx = getContext();
        BodyComponent head = new BodyComponent(getBitmapFromAsset(ctx, "head.png", AVATAR_HEAD), new CanvasPoint(0,0));
        head.addWearable(new WearableComponent(getBitmapFromAsset(ctx, hairName + ".png", AVATAR_HEAD), new CanvasPoint(0,0)));
        mBodyComponentFirst = head;
        head.setExtraOffset(5);
        if(head.getBitmap() == null) return;
        torso = new BodyComponent(getBitmapFromAsset(ctx, "torso.png", AVATAR_TORSO),
                new CanvasPoint(0, 0));
        torso.setExtraOffset(-30);
        torso.addWearable(new WearableComponent(getBitmapFromAsset(ctx, torsoName + ".png", AVATAR_TORSO), new CanvasPoint(0, 0)));
        head.setNext(torso);
        legs = new BodyComponent(getBitmapFromAsset(ctx, "legs.png", AVATAR_LEGS),
                new CanvasPoint(0, 0));
        legs.addWearable(new WearableComponent(getBitmapFromAsset(ctx, legsName + ".png", AVATAR_LEGS), new CanvasPoint(0, 0)));
        legs.setExtraOffset(-25);
        torso.setNext(legs);
        shoes = new BodyComponent(getBitmapFromAsset(ctx, shoesName + ".png", AVATAR_SHOES), new CanvasPoint(0, 0));
        legs.setNext(shoes);
        shoes.setExtraOffset(-20);
        legs.toogleZIndex();
    }

    public static Bitmap getBitmapFromAsset(Context context, String filePath, String rootFolder) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open("avatar/" + rootFolder + "/" + filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

    @Override
    public void onDraw(Canvas canvas) {

        mBodyComponentFirst.drawToCanvas(canvas, mPaint, 0);

//        super.onDraw(canvas);
    }

    private class CanvasPoint {
        int x, y;

        public CanvasPoint(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() { return x; }
        public int getY() { return y; }
    }

    private class BodyComponent {

        private Bitmap mBitmap;
        private CanvasPoint mCanvasPoint;

        int mExtraOffset = 0;

        private boolean isVisible;
        private boolean toggle;

        private BodyComponent mNextComponent;

        private List<WearableComponent> mComponents;

        public BodyComponent(Bitmap image, CanvasPoint point) {
            mBitmap = image;
            mCanvasPoint = point;
            isVisible = true;
            mComponents = new ArrayList<>();
            toggle = true;
        }

        public BodyComponent getNext() {
            return mNextComponent;
        }

        public void setNext(BodyComponent c) {
            mNextComponent = c;
        }

        public Bitmap getBitmap() { return mBitmap; }

        public CanvasPoint getCanvasPoint() { return mCanvasPoint; }

        public void setBitmap(Bitmap bmp) {
            mBitmap = bmp;
        }

        public void addWearable(WearableComponent wc) {
            mComponents.add(wc);
        }

//        public void drawToCanvas(Canvas canvas, Paint p, Rect src, Rect dest) {
//            canvas.drawBitmap(mBitmap, src, dest, p);
//            if(getNext() != null) {
//                getNext().drawToCanvas(canvas, p);
//            }
//            for(WearableComponent wc : mComponents) {
//                wc.drawToCanvas(canvas, p);
//            }
//        }


        public void setExtraOffset(int offset) {
            mExtraOffset = offset;
        }

        public void toogleZIndex() {
            toggle = !toggle;
        }

        public void drawToCanvas(Canvas canvas, Paint p, int offset) {

            if(mBitmap != null) {

                if (getNext() != null && toggle) {
                    getNext().drawToCanvas(canvas, p, mBitmap.getHeight() + offset + mExtraOffset);
                }

                int canvasW = canvas.getWidth();
                int canvasH = canvas.getHeight();
                mBitmap = Bitmap.createScaledBitmap(mBitmap, canvasW, mBitmap.getHeight() * canvasW / mBitmap.getWidth(), false);
                canvas.drawBitmap(mBitmap, 0, offset + mExtraOffset, p);
                Log.d("Draw", 0 + "," + (mBitmap.getHeight() + offset + mExtraOffset));
                for (WearableComponent wc : mComponents) {
                    wc.drawToCanvas(canvas, p, offset + mExtraOffset);
                }

                if (getNext() != null && !toggle) {
                    getNext().drawToCanvas(canvas, p, mBitmap.getHeight() + offset + mExtraOffset);
                }
            }
        }

        public boolean isVisible() { return isVisible; }
        public void setVisible(boolean visible) { isVisible = visible; }
    }

    private class WearableComponent {
        private Bitmap mBitmap;
        private CanvasPoint mCanvasPoint;
        int mWidth, mHeight;

        private boolean isVisible;

        public WearableComponent(Bitmap image, CanvasPoint point) {
            mBitmap = image;
            updateDimensions(mBitmap);
            mCanvasPoint = point;
            isVisible = true;
        }

        public Bitmap getBitmap() { return mBitmap; }

        private void updateDimensions(Bitmap bmp) {
            if(bmp != null) {
                mWidth = bmp.getWidth();
                mHeight = bmp.getHeight();
            }
        }

        public void setBitmap(Bitmap bmp) {
            mBitmap = bmp;
            updateDimensions(mBitmap);
        }

        public void drawToCanvas(Canvas canvas, Paint p, int offset) {
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            mBitmap = Bitmap.createScaledBitmap(mBitmap, width, mBitmap.getHeight() * width / mBitmap.getWidth(), false);
            Rect src = new Rect(0,0,mBitmap.getWidth(), mBitmap.getHeight());
            Rect dest = new Rect(mCanvasPoint.getX(),mCanvasPoint.getY() + offset, mBitmap.getWidth(), mBitmap.getHeight() + offset);

//            RectF rect = new RectF(mCanvasPoint.getX(),
//                                    mCanvasPoint.getY(),
//                                    canvas.getWidth(),
//                                    100 );
            canvas.drawBitmap(mBitmap, src, dest, p);
        }

        public boolean isVisible() { return isVisible; }
        public void setVisible(boolean visible) { isVisible = visible; }
    }
}
