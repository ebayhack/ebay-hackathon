package com.evatar.widget;

import android.animation.Animator;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;

public class CustomFloatingActionButton extends FloatingActionButton {

    boolean isAnimating = false;

    public CustomFloatingActionButton(Context context) {
        super(context);
    }

    public CustomFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Hides the floating action button
     */
    public void hide() {
        if(getVisibility() != View.GONE && !isAnimating) {
            animate().scaleX(0).scaleY(0).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) { isAnimating = true; }
                @Override
                public void onAnimationEnd(Animator animation) {
                    setVisibility(View.GONE);
                    setScaleX(0);
                    setScaleY(0);
                    isAnimating = false;
                }
                @Override
                public void onAnimationCancel(Animator animation) {}
                @Override
                public void onAnimationRepeat(Animator animation) {}

            }).setDuration(150).start();
        }
    }

    /**
     * Shows the floating action button
     */
    public void show() {
        if(getVisibility() != View.VISIBLE && !isAnimating) {
            setVisibility(View.VISIBLE);
            animate().scaleX(1).scaleY(1).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    isAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    setScaleX(1);
                    setScaleY(1);
                    isAnimating = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {}

            }).setDuration(150).start();
        }
    }
}
