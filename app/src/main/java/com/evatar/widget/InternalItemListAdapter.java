package com.evatar.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;

import com.evatar.ItemCardAdapter;
import com.evatar.R;
import com.evatar.parse.Item;
import com.evatar.parse.User;
import com.parse.ParseException;

import java.util.List;

/**
 * Created by miki on 12/13/15.
 */
public class InternalItemListAdapter extends ItemCardAdapter<Item>{

    ItemListener mListener;

    public InternalItemListAdapter(Context ctx, List<Item> listingData, User user) {
        super(ctx, listingData, user);
    }

    @Override
    public String getImageUrl(List<Item> items, int position) {
        return null;
    }

    @Override
    public Bitmap getImage(List<Item> items, int position) {
        try {
            return items.get(position).getImage(getContext());
        }  catch (ParseException ex) {
            ex.printStackTrace(); //TODO Handle
        }
        return null;
    }

    public void setItemListener(ItemListener l) {
        mListener = l;
    }

    @Override
    public void onBindCardData(@NonNull ItemCardAdapter.ViewHolder holder, int position) {
        holder.productBrand.setVisibility(View.GONE);
        final Item item = getItem(position);
        SpannableString restriction = new SpannableString(item.getName() + " (For levels " + item.getLevelResriction() + "+ )");
        if(mUser.getLevel() < item.getLevelResriction()) {
            restriction.setSpan(new ForegroundColorSpan(Color.RED), item.getName().length(), restriction.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            restriction.setSpan(new ForegroundColorSpan(Color.GREEN), item.getName().length(), restriction.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        holder.productName.setText(restriction);
        holder.productPrice.setText(String.valueOf(item.getCost()));
        holder.itemCurrencyImage.setVisibility(View.VISIBLE);
        holder.productImage.setMaxWidth(100);
        holder.productImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                String prompt = "Are you sure you want to purchase this item? ";
                SpannableString message = new SpannableString(prompt + item.getName());
                message.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), prompt.length(), message.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                builder.setMessage(message)
                    .setTitle("Purchase confirmation")
                    .setIcon(R.drawable.ic_action_shop)
                    .setPositiveButton("Buy it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (mListener != null) mListener.onItemPurchased(item);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                builder.show();
            }
        });
    }

    public interface ItemListener {
        void onItemPurchased(Item item);
    }
}
