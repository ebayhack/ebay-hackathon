package com.evatar.facebook;

public class FacebookGraphAPI {
    public static String ME = "me";
    public static String PROFILE_PICTURE = "picture";
    public static String FRIENDS = "friends";

    public static String generateUserCall(String userId, String... params) {
        String str = "/" + userId + generateCall(params);
        return str;
    }

    public static String generateCall(String... params) {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < params.length; i++) {
            builder.append("/");
            builder.append(params[i]);
        }
        return builder.toString();
    }
}
