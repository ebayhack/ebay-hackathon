package com.evatar.facebook;

/**
 * Created by miki on 11/25/15.
 */
public class FacebookFriend {

    private String name;
    private String id;
    private String imageUrl;

    public FacebookFriend(String name, String id, String imageUrl) {
        this.name = name;
        this.id = id;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Facebook Friend:" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", imageUrl='" + imageUrl + '\'';
    }
}
