package com.evatar.facebook;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.evatar.MainApplication;
import com.evatar.R;
import com.evatar.ViewUtils;
import com.evatar.parse.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by miki on 11/25/15.
 */
public class FriendListAdapter extends BaseAdapter {

    private List<FacebookFriend> mData;
    private Context mContext;
    private LayoutInflater mInflater;

    private MainApplication app;
    private User mUser;

    public FriendListAdapter(Activity a, List<FacebookFriend> data) {
        mData = data;
        mContext = a;
        app = (MainApplication)a.getApplication();
        mUser = app.getCurrentUser();
        mInflater = LayoutInflater.from(a);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public FacebookFriend getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(getItem(position).getId());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.friend_list_item, null);
        }

        if(convertView.getTag() == null) {
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        FacebookFriend friend = getItem(position);
        int size = (int)ViewUtils.convertDpToPixel(48, mContext);
        Picasso.with(mContext).load(friend.getImageUrl()).resize(size,size).into(holder.friendProfileImage);
        holder.friendName.setText(friend.getName());
        holder.friendActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUser.addUserFacebookFriend(getItem(position).getId());
                mData.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    class ViewHolder{
//        @Bind(R.id.friend_profile_image)
        public CircleImageView friendProfileImage;
//        @Bind(R.id.friend_name)
        public TextView friendName;
        public ImageButton friendActionButton;

        public ViewHolder(View v) {
            friendProfileImage = (CircleImageView)v.findViewById(R.id.friend_profile_image);
            friendName = (TextView)v.findViewById(R.id.friend_name);
            friendActionButton = (ImageButton)v.findViewById(R.id.friend_btn_action_add);
        }
    }
}
