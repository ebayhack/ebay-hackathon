package com.evatar;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evatar.ebayapi.EbayListing;
import com.evatar.ebayapi.EbaySearchRequest;
import com.evatar.ebayapi.EbaySearchResult;
import com.evatar.widget.CustomFloatingActionButton;
import com.evatar.widget.CustomRecyclerView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchItemsActivity extends BaseActivity {

    @Bind(R.id.fragment_category_fab)
    CustomFloatingActionButton mFAB;
    @Bind(R.id.fragment_category_recyclerview)
    CustomRecyclerView mRecyclerView;
    @Bind(R.id.toolbarInner)
    Toolbar mToolbar;
    @Bind(R.id.loading_layout)
    RelativeLayout loadingLayout;

    private EditText searchBoxEditText;


    private int mLastPageNum = 0, mNumItemsLoaded = 0;

    private boolean mIsContentLoaded = false, mIsContentLoading = false;

    private EbaySearchRequest searchRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_items);

        setToolbar(mToolbar);
        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);
        initSearchMode();
        toggleSearchMode(true);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void loadContent(final String keyword) {
        if(!mIsContentLoading) {
            mRecyclerView.setAdapter(null);
            loadingLayout.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
//            final int categoryId = getArguments().getInt(ARG_CATEGORY_ID);

            new AsyncTask<Void, Void, ArrayList<EbayListing>>() {
                @Override
                protected ArrayList<EbayListing> doInBackground(Void... params) {
                    mIsContentLoading = true;
                    searchRequest = new EbaySearchRequest(keyword);
                    EbaySearchResult res = new EbaySearchResult(SearchItemsActivity.this, searchRequest);
                    ArrayList<EbayListing> listings = res.getListings();
                    return listings;
                }

                @Override
                protected void onPostExecute(ArrayList<EbayListing> listings) {
                    mIsContentLoading = false;
                    mRecyclerView.setLayoutManager(new GridLayoutManager(SearchItemsActivity.this, 2));
                    mRecyclerView.setAdapter(new EbayItemListAdapter(SearchItemsActivity.this, listings));
                    mNumItemsLoaded += mRecyclerView.getAdapter().getItemCount();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            if (((GridLayoutManager) mRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() == mNumItemsLoaded - 1) {
                                new AsyncTask<Void, Void, ArrayList<EbayListing>>() {
                                    @Override
                                    protected ArrayList<EbayListing> doInBackground(Void... params) {
                                        searchRequest.incrementPage();
                                        EbaySearchResult res = new EbaySearchResult(SearchItemsActivity.this, searchRequest);
                                        ArrayList<EbayListing> listings = res.getListings();
                                        mNumItemsLoaded += listings.size();
                                        return listings;
                                    }

                                    @Override
                                    protected void onPostExecute(ArrayList<EbayListing> listings) {
                                        ((EbayItemListAdapter) mRecyclerView.getAdapter()).addAll(listings);
                                    }
                                }.execute(null, null, null);
                            }
                        }

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                        }
                    });
                    loadingLayout.setVisibility(View.GONE);
                    mIsContentLoaded = true;
                }
            }.execute(null, null, null);
        }
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Initiate all the search mode related views and
     * the search logic
     */
    private void initSearchMode() {
        searchBoxEditText = new EditText(this);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        searchBoxEditText.setMaxLines(1);
        searchBoxEditText.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_action_search), null, null, null);
        searchBoxEditText.setLayoutParams(params);
        searchBoxEditText.setVisibility(View.GONE);
        searchBoxEditText.setGravity(Gravity.CENTER_VERTICAL);
        searchBoxEditText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        searchBoxEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        searchBoxEditText.setFocusable(true);
        searchBoxEditText.setHint("Enter keyword");
        searchBoxEditText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        searchBoxEditText.setTextSize(17);
        searchBoxEditText.setFocusableInTouchMode(true);
        searchBoxEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    loadContent(searchBoxEditText.getText().toString());
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
        getToolbar().addView(searchBoxEditText);
        getToolbar().bringToFront();
    }

    /**
     * Toggles search mode on
     */
    private void toggleSearchModeOn() {
        getToolbar().getMenu().clear();
        searchBoxEditText.setVisibility(View.VISIBLE);
        searchBoxEditText.setFocusable(true);
        searchBoxEditText.requestFocus();
    }

    /**
     * Toggles search mode off
     */
    private void toggleSearchModeOff() {
        searchBoxEditText.setVisibility(View.GONE);
        getMenuInflater().inflate(R.menu.menu_search_items, getToolbar().getMenu());
    }

    /**
     *
     * @param activated true - will activate search mode\n false - will deactivate search mode
     * @return Whether the search mode was toggled
     */
    public boolean toggleSearchMode(boolean activated) {
        if(searchBoxEditText != null) {
            if (activated) {
                if (searchBoxEditText.getVisibility() == View.GONE) {
                    toggleSearchModeOn();
                    return true;
                }
            } else {
                if (searchBoxEditText.getVisibility() == View.VISIBLE) {
                    toggleSearchModeOff();
                    return true;
                }
            }
        }
        return false;
    }
}
