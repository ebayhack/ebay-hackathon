package com.evatar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.evatar.avatarview.AvatarView;
import com.evatar.ebayapi.EbayCategoryData;
import com.evatar.fragment.CategoryFragment;
import com.evatar.fragment.FeedFragment;
import com.evatar.fragment.MainPagerAdapter;
import com.evatar.parse.Avatar;
import com.evatar.parse.Item;
import com.evatar.parse.User;
import com.evatar.widget.CustomFloatingActionButton;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.parse.GetCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ViewPager mViewPager;
    private CustomFloatingActionButton mAvatarFAB, mEditAvatarFAB;
    private CardView mFloatingLevelCV, mFloatingPointsCV, mFloatingCoinsCV;
    private TextView mFloatingLevelTV, mFloatingPointsTV, mFloatingCoinsTV;
    private AvatarView mAvatarView;

    private User mUser;
    private MainApplication mainApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        mainApplication = (MainApplication)getApplication();

        mUser = mainApplication.getCurrentUser();

        if(mUser == null) {
            Intent login = new Intent(this, LoginActivity.class);
            startActivity(login);
            finish();
        } else {
            if(!mUser.isAvatarCreated()) {
                Intent register = new Intent(this, RegisterActivity.class);
                startActivity(register);
                finish();
            } else {

                SharedPreferences sp = getSharedPreferences("object", MODE_PRIVATE);
                sp.edit().clear().apply();
                PreferenceUtils.clear(AvatarView.AVATAR_TORSO);
                PreferenceUtils.clear(AvatarView.AVATAR_SHOES);
                PreferenceUtils.clear(AvatarView.AVATAR_LEGS);

                PreferenceUtils.putBoolean(RegisterActivity.USER_REGISTERED, true);

                mFloatingPointsTV.setText(mUser.getUserPoints() + "/" + mUser.getLevelMaxPoints() + " Xp");
                mFloatingLevelTV.setText(String.valueOf(mUser.getLevel()));
                mFloatingCoinsTV.setText(String.valueOf(mUser.getUserBalance()));
                mAvatarView.setVisibility(View.GONE);

                try {
                    initAvatar(mAvatarView, mUser.getAvatar());
                } catch (JSONException ex) {
                    ex.printStackTrace(); //TODO Handle
                }

                ViewUtils.overrideFonts(this, findViewById(R.id.activity_main_content), "fonts/gabriel.otf");
            }
        }
    }

    private void initAvatar(final AvatarView view, Avatar avatar) {
        view.setAvatarSkinColor(avatar.getSkinColorHex());
        view.setHeadBitmap(Utils.getBitmapFromAsset(this, "avatar/head/hair_" + avatar.getHaircutId() + ".png"), false);
        JSONArray extraArr = avatar.getExtraItems();
        for(int i = 0 ; i < extraArr.length(); i++) {
            try {
                final JSONObject extraObj = extraArr.getJSONObject(i);
                ParseQuery<Item> itemParseQuery = ParseQuery.getQuery(Item.class);
                itemParseQuery.getInBackground(extraObj.getString("Value"), new GetCallback<Item>() {
                    @Override
                    public void done(final Item object, ParseException e) {
                        if (e == null) {
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        view.setExtraImageView(extraObj.getString("Name"), object.getImage(MainActivity.this));
                                    } catch (Exception ex) {
                                        ex.printStackTrace(); //TODO Handle
                                    }
                                }
                            });
                        } else {
                            e.printStackTrace(); //TODO Handle
                        }
                    }
                });
            } catch (JSONException ex) {
                ex.printStackTrace(); //TODO Handle
            }

        }
    }

    @Nullable
    private <T extends Fragment> T createFragment(@Nullable String name, Class<T> c) {
        if(name == null) name = "Untitled";
        try {
            T f = c.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString(MainPagerAdapter.ARG_NAME, name);
            f.setArguments(bundle);
            return f;
        } catch(Exception ex) {
            ex.printStackTrace(); //TODO Handle
        }
        return null;
    }

    @Nullable
    private CategoryFragment createCategoryFragment(@Nullable String tag, int categoryId) {
        String name, metaTag = null, metaType = null;
        if(tag != null) {
            String[] data = tag.split(":");
            name = data[0];
            metaTag = data[1].split(";")[0];
            metaType = data[1].split(";")[1];
        } else {
            name = "Untitled";
        }
        CategoryFragment fragment = createFragment(name, CategoryFragment.class);
        Bundle args = fragment.getArguments();
        args.putInt(CategoryFragment.ARG_CATEGORY_ID, categoryId);
        args.putString(CategoryFragment.META_TAG, metaTag);
        args.putString(CategoryFragment.META_TYPE, metaType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initViews() {

        initAppCompatDesign((AppBarLayout) findViewById(R.id.toolbar_app_bar_layout));
        mAvatarFAB = (CustomFloatingActionButton) findViewById(R.id.view_avatar_fab);
        mAvatarFAB.setOnClickListener(this);
        mAvatarView = (AvatarView)findViewById(R.id.activity_main_avatar_view);
        mViewPager = (ViewPager) findViewById(R.id.activity_main_view_pager);

        Fragment[] fragments = new Fragment[1 + EbayCategoryData.categoryDataMap.size()];
        fragments[0] = createFragment("Feed", FeedFragment.class);
//        fragments[1] = createFragment("Home", HomeFragment.class);
        Iterator<Map.Entry<String, Integer>> iterator = EbayCategoryData.categoryDataMap.entrySet().iterator();
        int i = 1;
        while(iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            fragments[i] = createCategoryFragment(entry.getKey(), entry.getValue());
            i++;
        }

        PagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), fragments);

        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(10);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Fragment frag = ((MainPagerAdapter) mViewPager.getAdapter()).getItem(position);
                if (frag instanceof FeedFragment) {
                    ((FeedFragment)frag).loadContent(mUser);
                    mAvatarFAB.hide();
                    if(mAvatarView.getVisibility() == View.VISIBLE) {
                        mAvatarView.animate(new AvatarView.SimpleAnimateListener() {
                            @Override public void onFinished() {
                                mAvatarView.setVisibility(View.GONE);
                            }
                            @Override public void onStarted() {
                                mEditAvatarFAB.hide();
                            }
                        }).scaleY(0).scaleX(0).setDuration(150).start();
                    }
                } else if (mAvatarView.getVisibility() == View.GONE) mAvatarFAB.show();

                if (frag instanceof CategoryFragment) {
                    ((CategoryFragment) frag).loadContent();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setCurrentItem(1);
        setupTabsWithViewPager(mViewPager);
        setTabLayoutVisibility(View.VISIBLE);

        mFloatingCoinsCV = (CardView) findViewById(R.id.card_floating_coins);
        mFloatingLevelCV = (CardView) findViewById(R.id.card_floating_level);
        mFloatingPointsCV = (CardView) findViewById(R.id.card_floating_xp);

        mFloatingCoinsTV = (TextView) findViewById(R.id.floating_coins_tv);
        mFloatingLevelTV = (TextView) findViewById(R.id.floating_level_tv);
        mFloatingPointsTV = (TextView) findViewById(R.id.floating_xp_tv);

        mAvatarView = (AvatarView) findViewById(R.id.activity_main_avatar_view);

        mEditAvatarFAB = (CustomFloatingActionButton)findViewById(R.id.activity_main_edit_avatar_fab);
        mEditAvatarFAB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.view_avatar_fab:
                mAvatarFAB.hide();
                mAvatarView.animate(new AvatarView.SimpleAnimateListener() {
                    @Override public void onFinished() {

                    }
                    @Override public void onStarted() {
                        mAvatarFAB.setScaleX(0);
                        mAvatarFAB.setScaleY(0);
                        mAvatarView.setVisibility(View.VISIBLE);
                        mAvatarFAB.hide();
                        mEditAvatarFAB.show();
                    }
                }).scaleY(1).scaleX(1).setDuration(150).start();
                break;
            case R.id.activity_main_edit_avatar_fab:
                Intent editAvatar = new Intent(this, EditAvatarActivity.class);
                startActivity(editAvatar);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_checkout:
                startActivity(new Intent(this, ReviewAvatarActivity.class));
                return true;
            case R.id.action_search:
                //TODO Init search mode
                startActivity(new Intent(this, SearchItemsActivity.class));
                return true;
            case R.id.action_add_friends:
                Intent addFriendsIntent = new Intent(this, AddFriendsActivity.class);
                startActivity(addFriendsIntent);
                return true;
            case R.id.action_logout:
                logout();
                return true;
            case R.id.action_shop:
                Intent shopIntent = new Intent(this, ShopActivity.class);
                startActivity(shopIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    PreferenceUtils.clear("userObjectId");
                    mainApplication.setUser(null);
                    Intent login = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(login);
                    finish();
                } else {
                    e.printStackTrace();
                    Snackbar.make(mViewPager, "There's been an error logging out. Please try again.", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(mAvatarView.getVisibility() == View.VISIBLE) {
            mAvatarView.animate(new AvatarView.SimpleAnimateListener() {
                @Override public void onFinished() {
                    mAvatarFAB.show();
                    mAvatarView.setVisibility(View.GONE);
                }
                @Override public void onStarted() {
                    mEditAvatarFAB.hide();
                }
            }).scaleY(0).scaleX(0).setDuration(150).start();
        }
        else super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
        try {
            Utils.initAvatar(this, mAvatarView, mUser.getAvatar());
        } catch (JSONException ex ) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
}
