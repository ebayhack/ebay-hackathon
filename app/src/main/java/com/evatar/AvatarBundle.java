package com.evatar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.evatar.parse.Avatar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class AvatarBundle implements Iterable<Map.Entry<String, Bitmap>>, Comparable<AvatarBundle>{

    public static final String TAG_BUNDLE_DATE_CREATED = "dateCreated";
    public static final String TAG_BUNDLE_OWNER_NAME = "owner";
    public static final String TAG_BUNDLE_OWNER_ID = "ownerId";

    private static final String TAG_BUNDLE_AVATAR_DATA = "avatarData";

    private Date mDateCreated;
    private String avatarOwnerName;
    private String avatarOwnerId;
    private Map<String, Bitmap> mAvatarParts;
    private String photoUrl;

    private Avatar mAvatar;

    public AvatarBundle() {
        mAvatarParts = new LinkedHashMap<>();
    }

    public Bitmap getItem(String bodyTag) {
        return mAvatarParts.get(bodyTag);
    }

    public void putItems(Map<String, Bitmap> items) {
        mAvatarParts.putAll(items);
    }

    public void putItem(String bodyTag, Bitmap bmp) {
        mAvatarParts.put(bodyTag, bmp);
    }

    @Override
    public Iterator<Map.Entry<String, Bitmap>> iterator() {
        return mAvatarParts.entrySet().iterator();
    }


    @Override
    public int compareTo(AvatarBundle another) {
        return another.getDateCreated().compareTo(getDateCreated());
    }

    public void setOwnerName(String ownerName) { avatarOwnerName = ownerName; }

    public String getOwnerName() { return avatarOwnerName; }

    public void setDateCreated(Date date) {
        mDateCreated = date;
    }

    public Date getDateCreated() {
        return mDateCreated;
    }

    public void setOwnerId(String id) { avatarOwnerId = id; }

    public String getOwnerId() { return avatarOwnerId; }

    public Avatar getAvatar() { return mAvatar; }

    public void setAvatar(Avatar a) { mAvatar = a; }

    public String getPhotoUrl() { return photoUrl; }

    public void setPhotoUrl(String photo) { photoUrl = photo; }

    public static AvatarBundle fromJSON(JSONObject obj) throws JSONException{
        AvatarBundle bundle = new AvatarBundle();
        try {
            bundle.setDateCreated(ISO8601DateParser.parse(obj.getString(TAG_BUNDLE_DATE_CREATED)));
        } catch (ParseException ex) {
            ex.printStackTrace(); //TODO Handle
            bundle.setDateCreated(new Date());
        }
        bundle.setOwnerName(obj.getString(TAG_BUNDLE_OWNER_NAME));
        bundle.setOwnerId(obj.getString(TAG_BUNDLE_OWNER_ID));
        JSONArray arr = obj.getJSONArray(TAG_BUNDLE_AVATAR_DATA);
        Map<String, Bitmap> map = new LinkedHashMap<>();
        for(int i = 0; i < arr.length(); i ++) {
            JSONObject partObj = arr.getJSONObject(i);
            String name = partObj.getString("name");
            byte[] byteArray = partObj.getString("value").getBytes();
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            map.put(name, decodedBitmap);
        }

        bundle.putItems(map);

        return bundle;
    }

    public JSONObject toJSON() throws JSONException{
        JSONObject obj = new JSONObject();

        obj.put(TAG_BUNDLE_DATE_CREATED, ISO8601DateParser.toString(getDateCreated()));
        obj.put(TAG_BUNDLE_OWNER_NAME, getOwnerName());
        obj.put(TAG_BUNDLE_OWNER_ID, getOwnerId());

        JSONArray bodyPartJsonArray = new JSONArray();

        for(Map.Entry<String, Bitmap> bodyPart : this) {
            JSONObject partObj = new JSONObject();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bodyPart.getValue().compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            partObj.put("name", bodyPart.getKey());
            partObj.put("value", Base64.encodeToString(byteArray, Base64.DEFAULT));
            bodyPartJsonArray.put(partObj);
        }

        obj.put(TAG_BUNDLE_AVATAR_DATA, bodyPartJsonArray);

        return obj;
    }
}
