package com.evatar.parse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.evatar.ViewUtils;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by miki on 12/13/15.
 */
@ParseClassName("Item")
public class Item extends ParseObject {
    public static final String NAME = "name";
    public static final String IMAGE_URL = "imageUrl";
    public static final String IMAGE_FILE = "image";
    public static final String ITEM_ID = "itemId";
    public static final String COST = "cost";
    public static final String LEVEL_RESTRICTION = "levelRestriction";
    public static final String ITEM_TYPE = "type";

    public Item() {}

    public String getName() { return getString(NAME); }
    public String getImageUrl() {return getString(IMAGE_URL); }
    public int getItemId() { return getInt(ITEM_ID); }
    public int getCost() { return getInt(COST); }
    public int getLevelResriction() { return getInt(LEVEL_RESTRICTION); }

    public void setName(String name) { put(NAME, name); }

    public void setImageUrl(String url) { put(IMAGE_URL, url); }

    public void setItemId(int id) { put(ITEM_ID, id); }

    public void setCost(int cost) { put(COST, cost); }

    public void setLevelRestriction(int levelRestriction) { put(LEVEL_RESTRICTION, levelRestriction); }

    public void setImageFile(Bitmap image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        ParseFile file = new ParseFile(byteArray);
        put(IMAGE_FILE, file);
    }

    public Bitmap getImage(Context ctx) throws ParseException {
        ParseFile file = getParseFile(IMAGE_FILE);
        byte[] data = file.getData();
        int size = (int)ViewUtils.convertDpToPixel(30, ctx);
        return Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(data, 0, data.length), size, size, false);
    }

    public void setItemType(String tag) { put(ITEM_TYPE, tag); }

    public String getItemType() { return getString(ITEM_TYPE); }
}
