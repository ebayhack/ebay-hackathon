package com.evatar.parse;

import android.util.Log;

import com.evatar.AvatarBundle;
import com.parse.ParseClassName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@ParseClassName("_User")
public class User extends com.parse.ParseUser {
    public static final String USER_NAME = "username";
    public static final String USER_AGE = "age";
    public static final String USER_GENDER = "gender";
    public static final String USER_AVATAR = "avatar";
    public static final String UESR_POINTS = "points";
    public static final String USER_LEVEL = "level";
    public static final String USER_FB_ID = "facebookId";
    public static final String USER_BALANCE = "balance";
    public static final String USER_FB_FRIENDS = "facebookFriends";
    public static final String USER_AVATAR_CREATED = "avatarCreated";
    public static final String USER_AVATAR_BUNDLE_DATA = "userAvatarData";
    public static final String USER_ACQUIRED_ITEMS = "items";

    public static final int GENDER_MALE = 0;
    public static final int GENDER_FEMALE = 1;

    public User() {}

    public int getUserBalance() {
        return getInt(USER_BALANCE);
    }

    public void setUserBalance(int balance) {
        put(USER_BALANCE, balance);
    }

    public void setFacebookId(String fbId) { put(USER_FB_ID, fbId); }

    public String getFacebookId() {
        return getString(USER_FB_ID);
    }

    public int getAge() {
        return getInt(USER_AGE);
    }

    public void setAge(int age) { put(USER_AGE, age); }

    public int getGender() {
        return getInt(USER_GENDER);
    }

    public Avatar getAvatar() throws JSONException {
        List<Avatar> avatars = getAvatars();
        for(Avatar a : avatars) {
            if(a.isAvatarUsed()) {
                return a;
            }
        }
        return avatars.size() > 0 ? avatars.get(0) : null;
    }

    public List<Avatar> getAvatars() {
        List<Avatar> avatars = new ArrayList<>();
        JSONArray avatarsObj = getJSONArray(USER_AVATAR);
        for(int i = 0 ; i < avatarsObj.length();i++) {
            try {
                avatars.add(Avatar.fromJSON(avatarsObj.getJSONObject(i)));
            } catch (JSONException ex) {
                ex.printStackTrace(); //TODO Handle
            }
        }
        return avatars;
    }

    public JSONArray getAvatarsJsonArray() {
        JSONArray array = getJSONArray(USER_AVATAR);
        if(array == null) put(USER_AVATAR, new JSONArray());
        return array == null ? new JSONArray() : array;
    }

    public void setAvatar(Avatar a) throws JSONException{
        JSONArray avatars = getAvatarsJsonArray(); //TODO FIX
        int i;
        for(i = 0; i < avatars.length(); i++) {
            JSONObject avatarObj = avatars.getJSONObject(i);
            Avatar avatar = Avatar.fromJSON(avatarObj);
            if(avatar.getId() == a.getId()) {
                avatars.put(i, a.toJSON());
                put(USER_AVATAR, avatars);
                return;
            }
            i++;
        }
        for(i = 0; i < avatars.length(); i++) {
            JSONObject avatar = avatars.getJSONObject(i);
            avatar.put(Avatar.AVATAR_USED, false);
        }
        a.setAvatarUsed(true);
        avatars.put(a.toJSON());
        put(USER_AVATAR, avatars);
    }

    public List<AvatarBundle> getAvatarBundles() {
        List<AvatarBundle> bundles = new ArrayList<>();
        JSONArray avatarArray = getJSONArray(USER_AVATAR_BUNDLE_DATA);
        for(int i = 0; i < avatarArray.length(); i++) {
            try {
                bundles.add(AvatarBundle.fromJSON(avatarArray.getJSONObject(i)));
            } catch (JSONException ex) {
                Log.d("User", "getAvatarBundles iteration " + i + ": " + ex.getMessage());
            }
        }
        return bundles;
    }

    public void setUserPoints(int pts) { put(UESR_POINTS, pts); }

    public int getUserPoints() {
        return getInt(UESR_POINTS);
    }

    public void setLevel(int level) { put(USER_LEVEL, level); }

    public int getLevel() {
        return getInt(USER_LEVEL);
    }

    public int getLevelMaxPoints() {
        int l = getLevel();
        return l * 9 + (int)Math.pow(2, l - 1);
    }

    public List<String> getUserFacebookFriends() { return getList(USER_FB_FRIENDS); }

    public void setUserFacebookFriends(List<String> ids) {
        put(USER_FB_FRIENDS, ids);
    }

    public void addUserFacebookFriends(List<String> ids) {
        addAllUnique(USER_FB_FRIENDS, ids);
    }

    public void addUserFacebookFriend(String friendId) {
        addUnique(USER_FB_FRIENDS, friendId);
    }

    public boolean isFriend(String friendId) {
        return getUserFacebookFriends().contains(friendId);
    }

    public boolean isAvatarCreated() { return getBoolean(USER_AVATAR_CREATED); }

    public void setUserAvatarCreated(boolean created) { put(USER_AVATAR_CREATED, true); }

    public void setUserAcquiredItems(List<Integer> itemIds) {
        put(USER_ACQUIRED_ITEMS, itemIds);
    }

    public void addUserAcquiredItem(Integer itemId) {
        add(USER_ACQUIRED_ITEMS, itemId);
    }

    public boolean isItemAcquired(Integer id) {
        return getUserAcquiredItems().contains(id);
    }

    public List<Integer> getUserAcquiredItems() { return getList(USER_ACQUIRED_ITEMS); }
}
