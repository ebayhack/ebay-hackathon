package com.evatar.parse;

import android.graphics.Color;
import android.support.annotation.NonNull;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@ParseClassName("Avatar")
public class Avatar extends ParseObject implements Comparable<Avatar>{

    public static final String AVATAR_SKIN_COLOR = "skin_color";
    public static final String AVATAR_EYE_COLOR = "eye_color";
    public static final String AVATAR_HEIGHT = "height";
    public static final String AVATAR_WEIGHT = "weight";
    public static final String AVATAR_HAIR_COLOR = "hair_color";
    public static final String AVATAR_HAIRCUT_ID = "haircut_id";
    public static final String AVATAR_HAS_BEARD = "beard";
    public static final String AVATAR_DATA = "data";
    public static final String AVATAR_EXTRA_ITEMS = "extraItems";
    public static final String AVATAR_USED = "isUsed";
    public static final String ID = "id";

    public static final int HAIRCUT_FIRST = 1;
    public static final int HAIRCUT_SECOND = 2;
    public static final int HAIRCUT_THIRD = 3;


    @Override
    public int compareTo(@NonNull Avatar another) {
        return another.getCreatedAt().compareTo(getCreatedAt());
    }


    public Avatar() {}

    public String getSkinColorHex() { return getString(AVATAR_SKIN_COLOR); }

    public int getSkinColor() {
        return Color.parseColor(getString(AVATAR_SKIN_COLOR));
    }

    public void setSkinColor(String color) { put(AVATAR_SKIN_COLOR, color); }

    public int getEyeColor() {
        return Color.parseColor(getString(AVATAR_EYE_COLOR));
    }

    public void setEyeColor(String eyeColor) { put(AVATAR_EYE_COLOR, eyeColor); }

    public int getHeight() {
        return getInt(AVATAR_HEIGHT);
    }

    public void setHeight(int height) { put(AVATAR_HEIGHT, height); }

    public int getWeight() {
        return getInt(AVATAR_WEIGHT);
    }

    public int getHairColor() {
        return Color.parseColor(getString(AVATAR_HAIR_COLOR));
    }

    public int getHaircutId() {
        return getInt(AVATAR_HAIRCUT_ID);
    }

    public void setHaircut(int haircutId) { put(AVATAR_HAIRCUT_ID, haircutId); }

    public boolean hasBeard() {
        return getBoolean(AVATAR_HAS_BEARD);
    }

    public void setExtraItems(JSONArray items) {
        put(AVATAR_EXTRA_ITEMS, items);
    }

    public void setAvatarUsed(boolean used) { put(AVATAR_USED, used); }

    public boolean isAvatarUsed() { return getBoolean(AVATAR_USED); }

    public JSONArray getExtraItems() { return getJSONArray(AVATAR_EXTRA_ITEMS); }

    public void setExtraItem(Item item) {
        try {
            JSONArray extraItems = getExtraItems();
            boolean updated = false;
            for (int i = 0; i < extraItems.length(); i++) {
                JSONObject itemObj = extraItems.getJSONObject(i);
                if (itemObj.getString("Name").equals(item.getItemType())) {
                    itemObj.put("Value", item.getObjectId());
                    updated = true;
                }
            }
            if (!updated) {
                JSONObject itemObj = new JSONObject();
                itemObj.put("Name", item.getItemType());
                itemObj.put("Value", item.getObjectId());
                extraItems.put(itemObj);
            }
            setExtraItems(extraItems);
        } catch(JSONException ex) {
            ex.printStackTrace(); //TODO Handle
        }
    }

    public void setId(int id) { put(ID, id); }

    public int getId() { return getInt(ID); }

    public JSONObject toJSON() throws JSONException{
        JSONObject obj = new JSONObject();
        obj.put(AVATAR_SKIN_COLOR, getSkinColorHex());
        obj.put(AVATAR_HAIRCUT_ID, getHaircutId());
        obj.put(AVATAR_USED, isAvatarUsed());
        obj.put("extra_item_data", getExtraItems());
        obj.put(ID, getId());

        return obj;
    }

    public static Avatar fromJSON(JSONObject obj) throws JSONException{
        Avatar a = Avatar.create(Avatar.class);
        a.setSkinColor(obj.getString(AVATAR_SKIN_COLOR));
        a.setHaircut(obj.getInt(AVATAR_HAIRCUT_ID));
        a.setAvatarUsed(obj.getBoolean(AVATAR_USED));
        a.setId(obj.getInt(ID));
        a.setExtraItems(obj.getJSONArray("extra_item_data"));
        return a;
    }
}
