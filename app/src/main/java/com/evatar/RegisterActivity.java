package com.evatar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.evatar.avatarview.AvatarView;
import com.evatar.parse.Avatar;
import com.evatar.parse.User;
import com.parse.ParseException;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;
import butterknife.Bind;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    public static final String USER_REGISTERED = "user_registered";

    String SKIN_COLOR_1 = "ffe7a8";
    String SKIN_COLOR_2 = "ffcba8";
    String SKIN_COLOR_3 = "ffc279";
    String SKIN_COLOR_4 = "d9994e";
    String SKIN_COLOR_5 = "a96b46";
    String SKIN_COLOR_6 = "ad571d";
    String SKIN_COLOR_7 = "864317";
    String SKIN_COLOR_8 = "5a2e11";
    String SKIN_COLOR_9 = "2e1606";
    String SKIN_COLOR_10 = "180b02";

    MainApplication application;

    AvatarView avatarView;

    @Bind(R.id.register_pager_hair)
    ViewPager hairPager;
    @Bind(R.id.register_pager_mustache)
    ViewPager mustachePager;

    Button regBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        application = (MainApplication)getApplication();

        avatarView = (AvatarView) findViewById(R.id.register_avatar);
        CircleButton button1 = (CircleButton) findViewById(R.id.button1);
        button1.setColor(Color.parseColor("#" + SKIN_COLOR_1));
        button1.setOnClickListener(this);

        CircleButton button2 = (CircleButton) findViewById(R.id.button2);
        button2.setColor(Color.parseColor("#" + SKIN_COLOR_2));
        button2.setOnClickListener(this);

        CircleButton button3 = (CircleButton) findViewById(R.id.button3);
        button3.setColor(Color.parseColor("#" + SKIN_COLOR_3));
        button3.setOnClickListener(this);

        CircleButton button4 = (CircleButton) findViewById(R.id.button4);
        button4.setColor(Color.parseColor("#" + SKIN_COLOR_4));
        button4.setOnClickListener(this);

        CircleButton button5 = (CircleButton) findViewById(R.id.button5);
        button5.setColor(Color.parseColor("#" + SKIN_COLOR_5));
        button5.setOnClickListener(this);

        CircleButton button6 = (CircleButton) findViewById(R.id.button6);
        button6.setColor(Color.parseColor("#" + SKIN_COLOR_6));
        button6.setOnClickListener(this);

        CircleButton button7 = (CircleButton) findViewById(R.id.button7);
        button7.setColor(Color.parseColor("#" + SKIN_COLOR_7));
        button7.setOnClickListener(this);

        CircleButton button8 = (CircleButton) findViewById(R.id.button8);
        button8.setColor(Color.parseColor("#" + SKIN_COLOR_8));
        button8.setOnClickListener(this);

        CircleButton button9 = (CircleButton) findViewById(R.id.button9);
        button9.setColor(Color.parseColor("#" + SKIN_COLOR_9));
        button9.setOnClickListener(this);

        CircleButton button10 = (CircleButton) findViewById(R.id.button10);
        button10.setColor(Color.parseColor("#" + SKIN_COLOR_10));
        button10.setOnClickListener(this);

        regBtn = (Button)findViewById(R.id.btn_register_done);
        regBtn.setOnClickListener(this);

        ViewUtils.overrideFonts(this, findViewById(R.id.activity_main_content), "fonts/gabriel.otf");

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    private void register() {
        Avatar userAvatar = Avatar.create(Avatar.class);
        userAvatar.setSkinColor(avatarView.getAvatarSkinColor());
        userAvatar.setHaircut(Avatar.HAIRCUT_FIRST);
        userAvatar.setId(1);
        userAvatar.setExtraItems(new JSONArray());

        User mUser = application.getCurrentUser();
        if(mUser ==null) {
            //TOOD raise exception
        } else {
            mUser.setUserAvatarCreated(true);
            mUser.setUserAcquiredItems(new ArrayList<Integer>());
            mUser.setUserPoints(0);
            mUser.setUserBalance(30);
            mUser.setLevel(1);
            try {
                mUser.setAvatar(userAvatar);
            } catch (JSONException ex) {
                ex.printStackTrace(); //TODO Handle
                Toast.makeText(RegisterActivity.this, "Failed to update avatar", Toast.LENGTH_SHORT).show();
            }
            mUser.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null) {
                        Intent main = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(main);
                        finish();
                    } else {
                        e.printStackTrace(); //TODO Handle
                        regBtn.setClickable(true);
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_1);
                break;
            case R.id.button2:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_2);
                break;
            case R.id.button3:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_3);
                break;
            case R.id.button4:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_4);
                break;
            case R.id.button5:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_5);
                break;
            case R.id.button6:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_6);
                break;
            case R.id.button7:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_7);
                break;
            case R.id.button8:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_8);
                break;
            case R.id.button9:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_9);
                break;
            case R.id.button10:
                avatarView.setAvatarSkinColor("#99" + SKIN_COLOR_10);
                break;
            default:
                regBtn.setClickable(false);
                register();
                break;
        }
    }
}