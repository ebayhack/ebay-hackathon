package com.evatar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.evatar.avatarview.AvatarView;
import com.evatar.ebayapi.EbayAPI;
import com.evatar.ebayapi.EbayItem;
import com.evatar.parse.Avatar;
import com.evatar.parse.User;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReviewAvatarActivity extends BaseActivity implements View.OnClickListener{

    private static final String API_PRODUCTION = "http://open.api.ebay.com/shopping";
    private static final String URL_FORMAT = "?callname=GetSingleItem&responseencoding=JSON&appid=^1&siteid=0&version=847&ItemID=^2&IncludeSelector=ItemSpecifics,Details";


    private User mUser;
    private Avatar avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_avatar);

        mUser = ((MainApplication)getApplication()).getCurrentUser();
        if(mUser == null) {
            finish(); //TODO Raise Exception
        }
        try {
            avatar = mUser.getAvatar();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        SharedPreferences sp = getSharedPreferences("object" , Context.MODE_PRIVATE);

        String[] strs = {sp.getString("AVATAR_HEAD", "hair_2"), sp.getString("AVATAR_TORSO", "torso_1"),
                sp.getString("AVATAR_LEGS", "legs_1"), sp.getString("AVATAR_SHOES", "shoes_1")};

//        AvatarView view = (AvatarView)findViewById(R.id.avatar_view);
//        view.initFromAssets(strs[0], strs[1], strs[2], strs[3]);

        AvatarView avatarView = (AvatarView)findViewById(R.id.avatar_view);
        avatarView.setHeadBitmap(getBitmapFromAsset(this, avatar.getHaircutId() + ".png", AvatarView.AVATAR_HEAD), false);
//        avatarView.setTorsoBitmap(getBitmapFromAsset(this, strs[1] + ".png", AvatarView.AVATAR_TORSO), false);
//        avatarView.setLegsBitmap(getBitmapFromAsset(this, strs[2] + ".png", AvatarView.AVATAR_LEGS), false);
//        avatarView.setShoesBitmap(getBitmapFromAsset(this, strs[3] + ".png", AvatarView.AVATAR_SHOES), false);
        Utils.initAvatar(this, avatarView, avatar);


        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.review_avatar_fab);
        fab.setOnClickListener(this);

        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);

        String[] ids = {
                PreferenceUtils.readString(AvatarView.AVATAR_TORSO) != null ? PreferenceUtils.readString(AvatarView.AVATAR_TORSO).split(":")[2] : null,
                PreferenceUtils.readString(AvatarView.AVATAR_SHOES) != null ? PreferenceUtils.readString(AvatarView.AVATAR_SHOES).split(":")[2] : null,
                PreferenceUtils.readString(AvatarView.AVATAR_LEGS) != null ? PreferenceUtils.readString(AvatarView.AVATAR_LEGS).split(":")[2] : null,
        };

        final List<String> requests = new ArrayList<>();
        for(String id : ids) {
            if(id == null) continue;

            String request = API_PRODUCTION + TextUtils.expandTemplate(URL_FORMAT, EbayAPI.getAppId(), id).toString();
            requests.add(request);
//        Log.d(TAG, request);
        }

        new AsyncTask<Void, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(Void... params) {
                List<String> responses = new ArrayList<>();
                for(String req : requests) {
                    try {
                        responses.add(retreiveJsonForItem(req));
                    } catch (Exception ex) {
                        ex.printStackTrace(); //TODO Handle
                    }
                }
                return responses;
            }

            @Override protected void onPostExecute(List<String> s) {
                setup(s);
            }
        }.execute(null, null, null);



        ParseQuery<ParseObject> q = ParseQuery.getQuery("Items").whereContainedIn("asset_name", Arrays.asList(strs));
        List<ParseObject> lst = null;
        try {
            lst = q.find();

        } catch (ParseException ex) {
            ex.printStackTrace();
            finish();
        }


    }

    private void setup(List<String> responses) {

        List<EbayItem> items = new ArrayList<>();

        for(String res : responses) {
            try {
                JSONObject obj = new JSONObject(res);
                EbayItem mEbayItem = EbayItem.fromJson(obj);
                items.add(mEbayItem);


            } catch (JSONException ex) {
                ex.printStackTrace(); //TOOD Handle
            }
        }

        ListView listView = (ListView)findViewById(R.id.review_avatar_listview);
        listView.setAdapter(new RecyclerViewAdapter.ItemListAdapter(this, items));

        double sum = 0;
        if(items != null) {
            for(EbayItem obj : items) {
                sum += obj.getPrice();
            }
        }
        TextView totalTextView = (TextView)findViewById(R.id.review_avatar_total);
        totalTextView.setText("Total: " + sum + "$");

        ViewUtils.overrideFonts(this, findViewById(R.id.activity_review_avatar_content), "fonts/gabriel.otf");
    }

    private Bitmap getBitmapFromAsset(Context context, String filePath, String rootFolder) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open("avatar/" + rootFolder + "/" + filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, CheckoutActivity.class));
    }

    @Override
    protected void initViews() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_avatar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String retreiveJsonForItem(String request) throws Exception {

        String result = null;

        URL url = new URL(request);
        HttpURLConnection httpClient = (HttpURLConnection)url.openConnection();
        long length = url.toString().length();
        httpClient.connect();
        InputStream response;
        int status = httpClient.getResponseCode();

        if(status >= 400) {
            response = httpClient.getErrorStream();
//            Log.d(TAG, "Response error code " + status);
        } else {
            response = httpClient.getInputStream();
//            Log.d(TAG, "Response code " + status);
        }

        if(response != null){
            BufferedReader reader = new BufferedReader(new InputStreamReader(response));
            StringBuffer temp = new StringBuffer();
            String currentLine;

            while((currentLine = reader.readLine()) != null){
                temp.append(currentLine);
            }

            result=temp.toString();
            response.close();
        }

        httpClient.disconnect();

        return(result);

    }
}
