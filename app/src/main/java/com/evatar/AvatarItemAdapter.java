package com.evatar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.evatar.parse.Item;
import com.parse.ParseException;

import java.util.List;

/**
 * Created by miki on 12/15/15.
 */
public class AvatarItemAdapter extends RecyclerView.Adapter<AvatarItemAdapter.ItemVH> {
    private List<Item> itemList;
    private Context context;
    private ItemClickListener itemClickListener;

    public AvatarItemAdapter(Context c, List<Item> items) {
        context = c;
        itemList = items;
    }

    public void setOnItemClickListener(ItemClickListener listener) { itemClickListener = listener; }

    public Item getItem(int position) {
        return itemList.get(position);
    }

    @Override public ItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.avatar_item, parent, false);
        ItemVH holder = new ItemVH(view);
        return holder;
    }

    @Override public void onBindViewHolder(ItemVH holder, final int position) {
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemClickListener != null) {
                    itemClickListener.onItemClick(AvatarItemAdapter.this, position);
                }
            }
        });
        try { holder.itemImageButton.setImageBitmap(itemList.get(position).getImage(context)); }
        catch (ParseException ex) { ex.printStackTrace(); } //TODO Handle
    }

    @Override public int getItemCount() { return itemList.size(); }

    class ItemVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageButton itemImageButton;
        private View.OnClickListener mListener;
        public ItemVH(View view) {
            super(view);
            itemImageButton = (ImageButton)view.findViewById(R.id.avatar_item_image);
            itemImageButton.setOnClickListener(this);
        }
        public void setOnClickListener(View.OnClickListener listener) { mListener = listener; }
        @Override public void onClick(View v) {
            if(mListener != null) mListener.onClick(v);
        }
    }

    public interface ItemClickListener {
        void onItemClick(AvatarItemAdapter adapter, int position);
    }
}

