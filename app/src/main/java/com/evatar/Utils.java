package com.evatar;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

import com.evatar.avatarview.AvatarView;
import com.evatar.parse.Avatar;
import com.evatar.parse.Item;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by miki on 12/10/15.
 */
public class Utils {
    public static Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

    public static String parseDate(Date date) {
        Date now = new Date();

        if(date == null) {
            date = new Date();
        }

        int year = now.getYear() - date.getYear();
        int month = now.getMonth() - date.getMonth();
        int day = now.getDay() - date.getDay();
        int hour = now.getHours() - date.getHours();
        int minute = now.getMinutes() - date.getMinutes();
        int second = now.getSeconds() - date.getSeconds();

        StringBuilder builder = new StringBuilder();

        if(year > 0) builder.append(year).append(" Year").append(year > 1 ? "s" : "");
        else if(month > 0) builder.append(month).append(" Month").append(month > 1 ? "s" : "");
        else if(day > 0) builder.append(day).append(" Day").append(day > 1 ? "s" : "");
        else if(hour > 0) builder.append(hour).append(" Hour").append(hour > 1 ? "s" : "");
        else if(minute > 0) builder.append(minute).append(" Minute").append(minute > 1 ? "s" : "");
        else if(second > 0) builder.append(second).append(" Second").append(second > 1 ? "s" : "");

        return builder.toString();

    }

    public static Date generateRandomDate(int range) {
        Date now = new Date();
        int randYear = randBetween(now.getYear() - range, now.getYear());
        int randMonth = randBetween(1,12);
        int randDay = randBetween(1,30);
        int randHour = randBetween(1, 24);
        int randMinute = randBetween(1, 60);
        int randSecond = randBetween(1, 60);
        return new Date(randYear, randMonth, randDay, randHour, randMinute, randSecond);
    }

    private static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    public static void initAvatar(final Activity a, final AvatarView view, Avatar avatar) {
        view.setAvatarSkinColor(avatar.getSkinColorHex());
        view.setHeadBitmap(Utils.getBitmapFromAsset(a, "avatar/head/hair_" + avatar.getHaircutId() + ".png"), false);

        String[] metaStrings = {
                PreferenceUtils.readString(AvatarView.AVATAR_TORSO),
                PreferenceUtils.readString(AvatarView.AVATAR_LEGS),
                PreferenceUtils.readString(AvatarView.AVATAR_SHOES),
        };

        for(String metaString : metaStrings) {
            if(metaString != null) {
                String[] meta = metaString.split(":");
                Bitmap bitmap = Utils.getBitmapFromAsset(a, "avatar/template/" + meta[1] + ".png");

                int color = Integer.valueOf(meta[0]);

                BitmapDrawable drawable = new BitmapDrawable(bitmap);
//                drawable.setColorFilter();

                view.setAvatarPiece(drawable.getBitmap(), meta[1], false);
                view.getAvatarPieceImage(meta[1]).setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
//                view.getAvatarPieceImage(meta[1]).setImageDrawable(drawable);

//                changeBitmapColor(bitmap, view.getAvatarPieceImage(meta[1]), color);

//                view.setAvatarPiece(bitmap, meta[1], false);
            }
        }


        JSONArray extraArr = avatar.getExtraItems();
        for(int i = 0 ; i < extraArr.length(); i++) {
            try {
                final JSONObject extraObj = extraArr.getJSONObject(i);
                ParseQuery<Item> itemParseQuery = ParseQuery.getQuery(Item.class);
                itemParseQuery.getInBackground(extraObj.getString("Value"), new GetCallback<Item>() {
                    @Override
                    public void done(final Item object, ParseException e) {
                        if (e == null) {
                            a.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        view.setExtraImageView(extraObj.getString("Name"), object.getImage(a));
                                    } catch (Exception ex) {
                                        ex.printStackTrace(); //TODO Handle
                                    }
                                }
                            });
                        } else {
                            e.printStackTrace(); //TODO Handle
                        }
                    }
                });
            } catch (JSONException ex) {
                ex.printStackTrace(); //TODO Handle
            }

        }
    }

    public static void changeBitmapColor(Bitmap sourceBitmap, ImageView image, int color) {

        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
                sourceBitmap.getWidth() - 1, sourceBitmap.getHeight() - 1);
        Paint p = new Paint();
        ColorFilter filter = new LightingColorFilter(color, 1);
        p.setColorFilter(filter);
        image.setImageBitmap(resultBitmap);

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);
    }
}
