package com.evatar;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.evatar.avatarview.AvatarView;
import com.evatar.parse.Avatar;
import com.evatar.parse.Item;
import com.evatar.parse.User;
import com.evatar.widget.CustomFloatingActionButton;
import com.evatar.widget.CustomRecyclerView;
import com.evatar.widget.InternalItemListAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;

public class ShopActivity extends BaseActivity {

    private MainApplication app;
    private User mUser;

    @Bind(R.id.shop_recyclerview)
    CustomRecyclerView mRecyclerView;
    @Bind(R.id.toolbarInner)
    Toolbar mToolbar;
    @Bind(R.id.loading_layout)
    RelativeLayout mLoadingLayout;
    @Bind(R.id.view_avatar_fab)
    CustomFloatingActionButton mViewAvatarFAB;
    @Bind(R.id.activity_shop_avatar_view)
    AvatarView mAvatarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        app = (MainApplication)getApplication();
        mUser = app.getCurrentUser();
        if(mUser == null) {
            finish(); //TODO Raise exception
        }

        setStatusBarColor(R.color.colorPrimaryDark);
        setSupportActionBar(mToolbar);
        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        ParseQuery<Item> itemParseQuery = ParseQuery.getQuery(Item.class).whereNotContainedIn(Item.ITEM_ID, mUser.getUserAcquiredItems());
        itemParseQuery.findInBackground(new FindCallback<Item>() {
            @Override
            public void done(List<Item> objects, ParseException e) {
                if (e == null) {
                    mLoadingLayout.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    final InternalItemListAdapter adapter = new InternalItemListAdapter(ShopActivity.this, objects, mUser);
                    adapter.setItemListener(new InternalItemListAdapter.ItemListener() {
                        @Override
                        public void onItemPurchased(final Item item) {
                            int diff = mUser.getUserBalance() - item.getCost();
                            if(diff < 0 || mUser.getLevel() < item.getLevelResriction()) {
                                Snackbar.make(mRecyclerView, "You cannot buy this item :(", Snackbar.LENGTH_LONG).show();
                            } else {
                                mUser.setUserBalance(diff);
                                mUser.addUserAcquiredItem(item.getItemId());
                                try {
                                    Avatar avatar = mUser.getAvatar();

                                    mUser.setAvatar(avatar);
                                } catch (JSONException ex) {
                                    ex.printStackTrace(); //TODO Handle
                                }
                                mUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            try {
                                                mViewAvatarFAB.performClick();
                                                mAvatarView.setExtraImageView(item.getItemType(), item.getImage(ShopActivity.this));
                                                adapter.removeItem(item);
                                                Snackbar.make(mRecyclerView, "Congrats! You have purchased " + item.getName(), Snackbar.LENGTH_LONG).show();
                                            } catch (ParseException ex) {
                                                ex.printStackTrace(); //TODO Handle
                                            }
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    onBackPressed();
                                                }
                                            }, 2000);
                                        } else {
                                            e.printStackTrace(); //TODO Handle
                                        }
                                    }
                                });
                            }
                        }
                    });
                    mRecyclerView.setAdapter(adapter);
                    mRecyclerView.setItemAnimator(new ScaleInAnimator());
//                    mRecyclerView.setItemAnimator(new Sca);
                } else {
                    e.printStackTrace(); //TODO Handle
                }
            }
        });

        mViewAvatarFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewAvatarFAB.hide();
                mAvatarView.animate(new AvatarView.SimpleAnimateListener() {
                    @Override public void onFinished() {

                    }
                    @Override public void onStarted() {
                        mViewAvatarFAB.setScaleX(0);
                        mViewAvatarFAB.setScaleY(0);
                        mAvatarView.setVisibility(View.VISIBLE);
                        mViewAvatarFAB.hide();
                    }
                }).scaleY(1).scaleX(1).setDuration(150).start();
            }
        });

        try {
            Utils.initAvatar(this, mAvatarView, mUser.getAvatar());
        } catch (JSONException ex) {
            ex.printStackTrace();//TODO Handle
        }
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        if(mAvatarView.getVisibility() == View.VISIBLE) {
            mAvatarView.animate(new AvatarView.SimpleAnimateListener() {
                @Override public void onFinished() {
                    mViewAvatarFAB.show();
                    mAvatarView.setVisibility(View.GONE);
                }
                @Override public void onStarted() {}
            }).scaleY(0).scaleX(0).setDuration(150).start();
        }
        else super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shop, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
