package com.evatar;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.evatar.facebook.FacebookGraphAPI;
import com.evatar.parse.User;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener{

    @Bind(R.id.btnSignUp)
    Button mSignUpButton;
    @Bind(R.id.btnSingIn)
    Button mSignInButton;
    @Bind(R.id.login_username_edit_text)
    EditText mUsernameET;
    @Bind(R.id.login_password_edit_text)
    EditText mPasswordET;
    @Bind(R.id.login_connect_facebook_button)
    Button mFacebookLoginButton;

    private MainApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        app = (MainApplication)getApplication();

        mSignInButton.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);
        mFacebookLoginButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSingIn:
                //TODO Signin
                signIn();
                break;
            case R.id.btnSignUp:
                //TODO Signup
                signUp();
                break;
            case R.id.login_connect_facebook_button:
                signInWithFacebook();
                break;
        }
    }

    private void signIn() {
        User.logInInBackground(mUsernameET.getText().toString(), mPasswordET.getText().toString(), new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    // Hooray! The user is logged in.
                    Snackbar.make(mSignUpButton, "Welcome " + user.getUsername(), Snackbar.LENGTH_LONG).show();
                    User newUser = (User)user;
                    newUser.setUserFacebookFriends(new ArrayList<String>());
                    app.setUser(newUser);
                    Intent main = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
                } else {
                    // Signup failed. Look at the ParseException to see what happened.
                    e.printStackTrace(); //TODO Handle
                }
            }
        });
    }

    private void signUp() {
        Intent signUpIntent = new Intent(this, MainActivity.class); //TODO Replace with SingupActivity
        startActivity(signUpIntent);
    }

    private void signInWithFacebook() {
        List<String> permissions = Arrays.asList("user_friends", "user_likes", "user_photos");
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                } else if (user.isNew()) {
                    Log.d("MyApp", "User signed up and logged in through Facebook!");
                    final User newUser = (User)user;
//                    JSONObject authData = newUser.getJSONObject("authData");
//                    try {
//                        String fbId = authData.getJSONObject("facebook").getString("id");
//                        newUser.setFacebookId(fbId);
//                    } catch (JSONException ex) {
//                        //TODO Handle
//                        ex.printStackTrace();
//                    }
                    newUser.setUserFacebookFriends(new ArrayList<String>());
                    newUser.setLevel(1);
                    newUser.setUserPoints(0);
                    newUser.setUserBalance(50);
                    String request = FacebookGraphAPI.generateCall(FacebookGraphAPI.ME);
                    new GraphRequest(AccessToken.getCurrentAccessToken(), request, null, HttpMethod.GET, new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse graphResponse) {
                            try {
                                String id = graphResponse.getJSONObject().getString("id");
                                newUser.setFacebookId(id);
                                app.setUser(newUser);
                                Intent main = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(main);
                                finish();
                            } catch(Exception ex) {
                                ex.printStackTrace(); //TODO Handle
                                Toast.makeText(LoginActivity.this, "Error logging in with Facebook", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).executeAsync();

                } else {
                    Log.d("MyApp", "User logged in through Facebook!");
                    app.setUser((User)user);
                    Intent main = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(main);
                    finish();
//                    try {
//                        User newUser = ParseQuery.getQuery(User.class).get(user.getObjectId());
//                        app.setUser(newUser);
//                        Intent main = new Intent(LoginActivity.this, MainActivity.class);
//                        startActivity(main);
//                        finish();
//                    } catch(ParseException ex) {
//                        ex.printStackTrace(); //TODO Handle
//                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }

        return super.onOptionsItemSelected(item);
    }
}
