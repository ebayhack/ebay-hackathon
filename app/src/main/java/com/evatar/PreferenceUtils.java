package com.evatar;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

public class PreferenceUtils {

    private static SharedPreferences mPrefs;

    public static void init(Context c) {
        mPrefs = c.getSharedPreferences(c.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
    }

    public static void putString(String key, String s) {
        if(mPrefs == null) return; //TODO Handle
        mPrefs.edit().putString(key, s).apply();
    }

    public static void clearAll() {
        if(mPrefs == null) return; //TODO Handle
        mPrefs.edit().clear().apply();
    }

    public static void clear(String key) {
        if(mPrefs == null) return; //TODO Handle
        mPrefs.edit().remove(key).apply();
    }

    @Nullable
    public static String readString(String key) {
        if(mPrefs == null) return null; //TODO Handle
        return mPrefs.getString(key, null);
    }

    public static void putBoolean(String key, boolean b) {
        if(mPrefs == null) return; //TODO Handle
        mPrefs.edit().putBoolean(key, b).apply();
    }

    public static boolean readBoolean(String key) {
        if(mPrefs == null) return false; //TODO Handle
        return mPrefs.getBoolean(key, false);
    }

    public static void putInt(String key, int s) {
        if(mPrefs == null) return; //TODO Handle
        mPrefs.edit().putInt(key, s).apply();
    }

    public static int readInt(String key) {
        if(mPrefs == null) return -1; //TODO Handle
        return mPrefs.getInt(key, -1);
    }
}
