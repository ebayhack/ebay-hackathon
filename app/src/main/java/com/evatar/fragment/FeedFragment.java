package com.evatar.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evatar.AvatarBundle;
import com.evatar.FeedAdapter;
import com.evatar.R;
import com.evatar.ViewUtils;
import com.evatar.facebook.FacebookGraphAPI;
import com.evatar.parse.Avatar;
import com.evatar.parse.User;
import com.evatar.widget.CustomFloatingActionButton;
import com.evatar.widget.CustomRecyclerView;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miki on 11/18/15.
 */
public class FeedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private CustomFloatingActionButton mFAB;
    private CustomRecyclerView mRecyclerView;
    private SwipeRefreshLayout refreshLayout;

    boolean loaded = false;

    private User mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        // Inflate the layout for this fragment

        return view;
    }

    public void loadContent(final User mUser) {
        if(!loaded) {
            List<String> friendIds = mUser.getUserFacebookFriends();
            ParseQuery<User> userParseQuery = ParseQuery.getQuery(User.class).whereContainedIn(User.USER_FB_ID, friendIds);
            userParseQuery.findInBackground(new FindCallback<User>() {
                @Override
                public void done(List<User> objects, ParseException e) {
                    if (e == null) {
                        Log.d("Users Count", String.valueOf(objects.size()));
                        initFragment(objects);
                        if(FeedFragment.this.mUser == null) FeedFragment.this.mUser = mUser;
                        loaded = true;
                        refreshLayout.setRefreshing(false);
                    } else {
                        e.printStackTrace(); //TODO Handle
                    }
                }
            });
        }

//        String id = mUser.getFacebookId();
//        String request = FacebookGraphAPI.generateCall(FacebookGraphAPI.ME, FacebookGraphAPI.FRIENDS);
//        new GraphRequest(AccessToken.getCurrentAccessToken(), request, null, HttpMethod.GET, new GraphRequest.Callback() {
//            @Override
//            public void onCompleted(GraphResponse graphResponse) {
//
//            }
//        }).executeAsync();
    }

    private void initFragment(final List<User> users) {
        final List<AvatarBundle> data = new ArrayList<>();
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(new FeedAdapter(getActivity(), data));
            new AsyncTask<Void,Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    for(User user : users) {
                        try {
                            String request = FacebookGraphAPI.generateCall(user.getFacebookId());
                            GraphResponse friendResponse = new GraphRequest(AccessToken.getCurrentAccessToken(), request, null, HttpMethod.GET).executeAndWait();
                            String name = friendResponse.getJSONObject().getString("name");
                            String id = friendResponse.getJSONObject().getString("id");
                            Bundle bundle = new Bundle();
                            bundle.putInt("redirect", 0);
                            String photoRequest = FacebookGraphAPI.generateUserCall(id, FacebookGraphAPI.PROFILE_PICTURE);
                            GraphResponse photoResponse = new GraphRequest(AccessToken.getCurrentAccessToken(), photoRequest, bundle, HttpMethod.GET).executeAndWait();
                            Log.d("JSON", photoResponse.getRawResponse());
                            String photoUrl = photoResponse.getJSONObject().getJSONObject("data").getString("url");
                            Avatar userAvatar = user.getAvatar();
                            AvatarBundle avatarBundle = new AvatarBundle();
//                bundle.setDateCreated(Utils.generateRandomDate(2));
                            avatarBundle.setDateCreated(user.getCreatedAt());
                            avatarBundle.setOwnerName(name);
                            avatarBundle.setOwnerId(id);
                            avatarBundle.setAvatar(userAvatar);
                            avatarBundle.setPhotoUrl(photoUrl);
                            data.add(avatarBundle);
                        } catch (JSONException ex) {
                            ex.printStackTrace(); //TODO Handle
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    Log.d("SIZE", String.valueOf(data.size()));
                    final FeedAdapter adapter = new FeedAdapter(getActivity(), data);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                mRecyclerView.setLayoutManager(layoutManager);
                            ((FeedAdapter) mRecyclerView.getAdapter()).addAll(data);
//                mRecyclerView.setAdapter(adapter);
                            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                    super.onScrollStateChanged(recyclerView, newState);
                                }

                                @Override
                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                    super.onScrolled(recyclerView, dx, dy);
                                    float pixelOffset = ViewUtils.convertDpToPixel(196, getContext());
                                    int offset = ((CustomRecyclerView) recyclerView).getVerticalScrollOffset();
                                    if (offset > pixelOffset) {
                                        mFAB.show();
                                    } else if (offset <= 100) {
                                        mFAB.hide();
                                    }
                                }
                            });
                            refreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.feed_refresh_layout);
                            refreshLayout.setOnRefreshListener(FeedFragment.this);
                        }
                    });
                }
            }.execute(null, null, null);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Context ctx = getActivity();
        final View view = getView();

        mFAB = (CustomFloatingActionButton)view.findViewById(R.id.fragment_feed_fab);
        mRecyclerView = (CustomRecyclerView)view.findViewById(R.id.fragment_feed_recyclerview);
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.feed_refresh_layout);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });
//        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                if (position == 0) return layoutManager.getSpanCount();
//                return 1;
//            }
//        });
    }

    @Override
    public void onRefresh() {
        if(mUser != null) {
            loaded = false;
            mRecyclerView.setAdapter(null);
            loadContent(mUser);
        }
    }
}
