package com.evatar.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evatar.R;
import com.evatar.RecyclerViewAdapter;
import com.evatar.ViewUtils;
import com.evatar.widget.CustomFloatingActionButton;
import com.evatar.widget.CustomRecyclerView;

/**
 * Created by miki on 11/18/15.
 */
public class HomeFragment extends Fragment{

    private CustomFloatingActionButton mFAB;
    private CustomRecyclerView mRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        // Inflate the layout for this fragment

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Context ctx = getActivity();
        final View view = getView();

        mFAB = (CustomFloatingActionButton)view.findViewById(R.id.activity_main_fab);
        mRecyclerView = (CustomRecyclerView)view.findViewById(R.id.activity_main_recyclerview);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });
        final GridLayoutManager layoutManager = new GridLayoutManager(ctx, 2);
//        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                if (position == 0) return layoutManager.getSpanCount();
//                return 1;
//            }
//        });

        mRecyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(ctx);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                float pixelOffset = ViewUtils.convertDpToPixel(196, ctx);
                int offset = ((CustomRecyclerView) recyclerView).getVerticalScrollOffset();
                if (offset > pixelOffset) {
//                if(mRecyclerViewTotalScrollOffset > 200) {
//                    mFAB.setVisibility(View.VISIBLE);
                    mFAB.show();
//                    mAvatarContainer.setVisibility(View.GONE);
//                    ViewUtils.collapse(mAvatarContainer);
                } else if (offset <= 100) {
//                    mFAB.setVisibility(View.GONE);
                    mFAB.hide();
//                    mAvatarContainer.setVisibility(View.VISIBLE);
//                    ViewUtils.expand(mAvatarContainer);
                }
            }
        });
    }
}
