package com.evatar.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.evatar.EbayItemListAdapter;
import com.evatar.R;
import com.evatar.ViewUtils;
import com.evatar.ebayapi.EbayListing;
import com.evatar.ebayapi.EbaySearchCategoryRequest;
import com.evatar.ebayapi.EbaySearchResult;
import com.evatar.widget.CustomFloatingActionButton;
import com.evatar.widget.CustomRecyclerView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CategoryFragment extends Fragment{

    public static final String ARG_CATEGORY_ID = "ebayCategoryId";
    public static final String META_TAG = "metaTag";
    public static final String META_TYPE = "metaType";


    @Bind(R.id.fragment_category_fab)
    CustomFloatingActionButton mFAB;
    @Bind(R.id.fragment_category_recyclerview)
    CustomRecyclerView mRecyclerView;

    @Bind(R.id.loading_layout)
    RelativeLayout loadingLayout;

    private int mLastPageNum = 0, mNumItemsLoaded = 0;

    private boolean mIsContentLoaded = false, mIsContentLoading = false;

    private EbaySearchCategoryRequest categoryRequest;

    private String metaTag, metaType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void loadContent() {
        if(!mIsContentLoaded && !mIsContentLoading && getContext() != null) {

            final int categoryId = getArguments().getInt(ARG_CATEGORY_ID);
            metaTag = getArguments().getString(META_TAG);
            metaType = getArguments().getString(META_TYPE);

            new AsyncTask<Void, Void, ArrayList<EbayListing>>() {
                @Override
                protected ArrayList<EbayListing> doInBackground(Void... params) {
                    mIsContentLoading = true;
                    categoryRequest = new EbaySearchCategoryRequest(categoryId);
                    EbaySearchResult res = new EbaySearchResult(getContext(), categoryRequest);
                    ArrayList<EbayListing> listings = res.getListings();
                    return listings;
                }

                @Override
                protected void onPostExecute(ArrayList<EbayListing> listings) {
                    mIsContentLoading = false;
                    mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
                    mRecyclerView.setAdapter(new EbayItemListAdapter(getContext(), listings));
                    mNumItemsLoaded += mRecyclerView.getAdapter().getItemCount();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            if (((GridLayoutManager) mRecyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() == mNumItemsLoaded - 1) {
                                new AsyncTask<Void, Void, ArrayList<EbayListing>>() {
                                    @Override
                                    protected ArrayList<EbayListing> doInBackground(Void... params) {
                                        categoryRequest.incrementPage();
                                        EbaySearchResult res = new EbaySearchResult(getContext(), categoryRequest);
                                        ArrayList<EbayListing> listings = res.getListings();
                                        mNumItemsLoaded += listings.size();
                                        return listings;
                                    }

                                    @Override
                                    protected void onPostExecute(ArrayList<EbayListing> listings) {
                                        ((EbayItemListAdapter) mRecyclerView.getAdapter()).addAll(listings);
                                    }
                                }.execute(null, null, null);
                            }
                        }

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                        }
                    });
                    loadingLayout.setVisibility(View.GONE);
                    mIsContentLoaded = true;
                }
            }.execute(null, null, null);
        }
    }

    public String getMetaTag() { return metaTag; }
    public String getMetaType() { return metaType; }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                float pixelOffset = ViewUtils.convertDpToPixel(196, getContext());
                int offset = ((CustomRecyclerView) recyclerView).getVerticalScrollOffset();
                if (offset > pixelOffset) {
                    mFAB.show();
                } else if (offset <= 100) {
                    mFAB.hide();
                }
            }
        });
    }
}
