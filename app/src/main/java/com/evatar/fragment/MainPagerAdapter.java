package com.evatar.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by miki on 11/18/15.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final String ARG_NAME = "fragmentName";

    private Fragment[] mFragments;
    private String[] titles;

    public MainPagerAdapter(FragmentManager manager, Fragment[] fragments) {
        super(manager);
        mFragments = fragments;
        titles = new String[mFragments.length];
        int i = 0;
        for(Fragment f : mFragments) {
            Bundle bundle =  f.getArguments();
            if(bundle == null) continue;
            String title = bundle.getString(ARG_NAME);
            if(title == null) {
                titles[i] = "Untitled";
            } else {
                titles[i] = title;
            }
            i++;
        }
    }

    public MainPagerAdapter(FragmentManager manager, Fragment[] fragments, String[] titles) {
        super(manager);
        mFragments = fragments;
        this.titles = titles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override public Fragment getItem(int position) { return mFragments[position]; }
    @Override public int getCount() { return mFragments.length; }
}
