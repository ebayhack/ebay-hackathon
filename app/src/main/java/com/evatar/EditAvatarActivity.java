package com.evatar;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.evatar.avatarview.AvatarView;
import com.evatar.parse.Avatar;
import com.evatar.parse.Item;
import com.evatar.parse.User;
import com.evatar.widget.CustomRecyclerView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import org.json.JSONException;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditAvatarActivity extends BaseActivity implements View.OnClickListener{

    @Bind(R.id.toolbarInner)
    Toolbar mToolbar;
    @Bind(R.id.edit_action_sword)
    ImageButton mEditActionSwordButton;
    @Bind(R.id.edit_action_shield)
    ImageButton mEditActionShieldButton;
    @Bind(R.id.edit_avatar_main)
    AvatarView mAvatarView;
    @Bind(R.id.edit_avatar_recyclerview)
    CustomRecyclerView mAvatarRecyclerView;

    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_avatar);
        mUser = ((MainApplication)getApplication()).getCurrentUser();
        if(mUser == null) {
            finish(); //TODO Raise exception
        }

        setSupportActionBar(mToolbar);

        mEditActionShieldButton.setOnClickListener(this);
        mEditActionSwordButton.setOnClickListener(this);

        try {
            Utils.initAvatar(this, mAvatarView, mUser.getAvatar());
        } catch(JSONException ex) {
            ex.printStackTrace(); //TODO Handle
        }

        AvatarsAdapter adapter = new AvatarsAdapter(this, mUser.getAvatars());
        adapter.setOnItemClickListener(new AvatarsAdapter.ItemClickListener() {
            @Override
            public void onItemClick(AvatarsAdapter adapter, int position) {
                Avatar selected = adapter.getItem(position);
                try {
                    mUser.setAvatar(selected);
                    Utils.initAvatar(EditAvatarActivity.this, mAvatarView, selected);
                } catch (JSONException ex) {
                    ex.printStackTrace(); //TODO Handle
                }
            }
        });
        mAvatarRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mAvatarRecyclerView.setAdapter(adapter);

        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_avatar, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_action_shield:
                showSelectItemDialog("rightHandExtra");
                break;
            case R.id.edit_action_sword:
                showSelectItemDialog("leftHandExtra");
                break;
        }
    }

    private void showSelectItemDialog(String tag) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_view_items);
        dialog.setTitle("Select Items:");
//        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Widget_AlertDialog);
//        builder
//        .setView(LayoutInflater.from(this).inflate(R.layout.dialog_view_items, null))
//        .setTitle("Select and item:")
//        .setPositiveButton("Equip", null)
//        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });

//        final AlertDialog dialog = builder.create();
//        dialog.setContentView(R.layout.dialog_view_items);
        final CustomRecyclerView recyclerView = (CustomRecyclerView)dialog.findViewById(R.id.dialog_view_items_recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        ParseQuery<Item> itemParseQuery = ParseQuery.getQuery(Item.class).whereMatches(Item.ITEM_TYPE, tag).whereContainedIn(Item.ITEM_ID, mUser.getUserAcquiredItems());
        itemParseQuery.findInBackground(new FindCallback<Item>() {
            @Override
            public void done(List<Item> objects, ParseException e) {
                if(e == null) {
                    AvatarItemAdapter adapter = new AvatarItemAdapter(EditAvatarActivity.this, objects);
                    adapter.setOnItemClickListener(new AvatarItemAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(AvatarItemAdapter adapter, int position) {
                            try {
                                Avatar a = mUser.getAvatar();
                                Item item = adapter.getItem(position);
                                a.setExtraItem(item);
                                mUser.setAvatar(a);
                                try {
                                    mAvatarView.setExtraImageView(item.getItemType(), item.getImage(EditAvatarActivity.this));
                                } catch (ParseException ex) {
                                    ex.printStackTrace(); //TODO Handle
                                }
                                dialog.cancel();
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                dialog.cancel();
                            }
                        }
                    });
                    recyclerView.setAdapter(adapter);
                    dialog.show();
                } else {
                    e.printStackTrace(); //TODO Handle
                }
            }
        });

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Snackbar.make(findViewById(R.id.activity_main_content), "Saving profile...", Snackbar.LENGTH_LONG).show();
        try {
            mUser.save();
        } catch (ParseException ex) {
            ex.printStackTrace(); //TODO Handle
        }
        super.onBackPressed();
    }
}
