package com.evatar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.evatar.ebayapi.EbayListing;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by miki on 11/27/15.
 */
public class EbayItemListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int TYPE_ITEM = 1;

    private Context mContext;

    private int lastPosition = -1;

    private List<EbayListing> mListingData;
    private List<Bitmap> mImageData;

    private EbayItemListAdapter() {}

    public EbayItemListAdapter(Context ctx,final List<EbayListing> listingData) {
        mContext = ctx;
        if(listingData != null) {
            mListingData = listingData;
        } else {
            mListingData = new ArrayList<>();
        }
        mImageData = new ArrayList<>(mListingData.size());
        for(int i = 0; i < mListingData.size(); i ++) mImageData.add(null);
    }

    public void addAll(List<EbayListing> listings) {
        int size = getItemCount();
        mListingData.addAll(listings);
        notifyItemRangeInserted(size, mListingData.size() - 1);
    }

    @SuppressLint("NewApi") @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        if (type == TYPE_ITEM) {
            View itemView = inflater.inflate(R.layout.card_item, viewGroup, false);
            return new ViewHolder(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + type + " + make sure you're using types correctly");
    }final

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewHolder) {
            final ViewHolder vh = ((ViewHolder) holder);
            setAnimation(vh.rootView, position);
            vh.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent viewItem = new Intent(mContext, ViewEbayItemActivity.class);
                    viewItem.putExtra("itemId", getItem(position).getId());
                    Palette.from(mImageData.get(position)).generate(new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            viewItem.putExtra("paletteColor", palette.getVibrantColor(mContext.getResources().getColor(R.color.colorPrimary)));
                            mContext.startActivity(viewItem);
                        }
                    });
                }
            });
            vh.productName.setText(getItem(position).getTitle());
            String imageUrl = getItem(position).getImageUrl();
            if(imageUrl != null) {
                imageUrl = imageUrl.replaceAll("\\\\", "");
                if (mImageData.get(position) == null) {
                    Picasso.with(mContext).load(imageUrl).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            mImageData.add(position, bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                    Picasso.with(mContext).load(imageUrl).error(R.mipmap.ic_launcher).into(vh.productImage);
                } else {
                    vh.productImage.setImageBitmap(mImageData.get(position));
                }
            }
            vh.productPrice.setText(getItem(position).getCurrentPrice());

        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.recyclerview_item_enter);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    private EbayListing getItem(int position) {
        return mListingData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mListingData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.card_product_image_view)
        public ImageView productImage;
        @Bind(R.id.card_product_name_textview)
        public TextView productName;
        @Bind(R.id.card_product_brand_textview)
        public TextView productBrand;
        @Bind(R.id.card_product_price_textview)
        public TextView productPrice;
        @Bind(R.id.card_product)
        public CardView rootView;

        private ItemClickListener mListener;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) mListener.onClick(v);
                }
            });
        }

        public void setClickListener(ItemClickListener clickListener) {
            mListener = clickListener;
        }
    }

    public interface ItemClickListener {
        void onClick(View view);
    }
}
