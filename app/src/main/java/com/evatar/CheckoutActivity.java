package com.evatar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evatar.parse.User;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.Random;

public class CheckoutActivity extends BaseActivity implements Runnable {

    private ImageView coinsImageView;
    private ProgressBar progressBar;
    private TextView coinsTextView;
    private LinearLayout checkoutLayout;

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        mUser = ((MainApplication)getApplication()).getCurrentUser();

        setDisplayHomeAsUpEnabled(true, R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.color.textColorPrimary);

        new Handler().postDelayed(this, 3500);
    }

    @Override
    public void run() {
        progressBar.setVisibility(View.GONE);
        Random rng = new Random();
        final int coins = rng.nextInt(50);
        coinsTextView.setText("Hurray! You received " + coins + " coins!");
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                checkoutLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                checkoutLayout.setScaleY(1);
                checkoutLayout.setScaleX(1);

                mUser.increment("balance", coins);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                startActivity(new Intent(CheckoutActivity.this, MainActivity.class));
                                finish();
                            }
                        });
                    }
                }, 500);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        checkoutLayout.startAnimation(anim);
    }

    @Override
    protected void initViews() {
        coinsImageView = (ImageView) findViewById(R.id.checkout_coins_imageview);
        progressBar = (ProgressBar) findViewById(R.id.checkout_progressbar);
        coinsTextView = (TextView) findViewById(R.id.checkout_coins_textview);
        checkoutLayout = (LinearLayout) findViewById(R.id.checkout_wrapper);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checkout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
