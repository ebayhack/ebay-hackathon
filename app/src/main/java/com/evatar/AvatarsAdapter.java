package com.evatar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evatar.avatarview.AvatarView;
import com.evatar.parse.Avatar;

import java.util.List;

/**
 * Created by miki on 12/15/15.
 */
public class AvatarsAdapter extends RecyclerView.Adapter<AvatarsAdapter.AvatarVH> {
    private List<Avatar> itemList;
    private Context context;
    private ItemClickListener itemClickListener;

    public AvatarsAdapter(Context c, List<Avatar> items) {
        context = c;
        itemList = items;
    }

    public void setOnItemClickListener(ItemClickListener listener) { itemClickListener = listener; }

    public Avatar getItem(int position) {
        return itemList.get(position);
    }

    @Override public AvatarVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.avatar_layout, parent, false);
        AvatarVH holder = new AvatarVH(view);
        return holder;
    }

    @Override public void onBindViewHolder(AvatarVH holder, final int position) {
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemClickListener != null) {
                    itemClickListener.onItemClick(AvatarsAdapter.this, position);
                }
            }
        });

//        try { holder.avatarView.setImageBitmap(itemList.get(position).getImage(context)); }
//        catch (ParseException ex) { ex.printStackTrace(); } //TODO Handle
    }

    @Override public int getItemCount() { return itemList.size(); }

    class AvatarVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AvatarView avatarView;
        private View.OnClickListener mListener;
        public AvatarVH(View view) {
            super(view);
            avatarView = (AvatarView)view.findViewById(R.id.avatar_layout_view);
            avatarView.setOnClickListener(this);
        }
        public void setOnClickListener(View.OnClickListener listener) { mListener = listener; }
        @Override public void onClick(View v) {
            if(mListener != null) mListener.onClick(v);
        }
    }

    public interface ItemClickListener {
        void onItemClick(AvatarsAdapter adapter, int position);
    }
}

